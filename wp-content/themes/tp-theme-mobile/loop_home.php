<?php
/**
 * Default Loop
 *
 * @subpackage  shprink_one
 * @since       1.0
 */
$displayedOnSlideshow = 0;
if (is_category()){
    $option_slideshow = shprinkone_get_theme_option('theme_slideshow_category');
} else if (is_tag()){
    $option_slideshow = shprinkone_get_theme_option('theme_slideshow_tag');
} else if (is_front_page()){
	//Lấy các post có tag là home-slide cho vào slide.
	$args = array(
		'tag' => 'home-slide',
		'post_status' => 'publish'
	);
	$my_query = new WP_Query($args);
    //$option_slideshow = shprinkone_get_theme_option('theme_slideshow');
}
$postCount = count($wp_query->get_posts());
?>
<?php // if ($my_query->have_posts() && get_query_var('paged') < 2) : ?>
	<div class="container-slideshow">
		<div id="slideshow" class="carousel slide">
			<!-- Carousel items -->
			<div class="carousel-inner">
				<!-- Start the Loop. -->
				<?php while ($my_query->have_posts() ) : $my_query->the_post(); ?>
					<div <?php
					$classes = 'item';
					if ($displayedOnSlideshow === 0)
						$classes .= ' active'; post_class($classes)
					?>>
                    <?php if (has_post_thumbnail()): ?>
                        <a class="" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                            <?php //Vì đây là giao diện mobile nên không cho hiện thumnnail. ?>
                            <?php  the_post_thumbnail('post-image-mansory', array('class' => 'img-responsive img-full')); ?>
                        </a>
                        <h2 class="post-title media-heading">
											<a href="<?php the_permalink() ?>"
											   title="<?php echo the_title_attribute(); ?>"><?php the_title(); ?>
			</a>
		        </h2>
                        <div class="post-content hidden-xs">
												<?php the_excerpt(); ?>
										</div>
										<div class="post-content visible-xs">
											<?php $excerpt = get_the_excerpt() ?>
											<?php echo ( $excerpt != '' ) ? substr($excerpt, 0, 100) . ' [...]' : '' ?>
										</div>
                    <?php endif; ?>
                    <!--
						<div class="container">
							<div class="carousel-caption">
								<div class="media">
									<div class="media-body">
                                        <div class="title-wrapper">
                                        <?php if (has_post_thumbnail()): ?>
                                            <a class="post-thumbnail" href="<?php the_permalink() ?>">
                                            	<?php //Vì đây là giao diện mobile nên không cho hiện thumnnail. ?>
                                                <?php // the_post_thumbnail('post-image-mansory', array('class' => 'img-thumbnail img-responsive')); ?>
                                            </a>
                                        <?php endif; ?>
										<h2 class="post-title media-heading">
											<a href="<?php the_permalink() ?>"
											   title="<?php echo the_title_attribute(); ?>"><?php the_title(); ?>
											</a>
										</h2>
                                        <?php echo shprinkone_get_post_meta(true, true, true, false, false, true, true) ?>
                                        </div>


										<div class="post-content hidden-xs">
												<?php the_excerpt(); ?>
										</div>
										<div class="post-content visible-xs">
											<?php $excerpt = get_the_excerpt() ?>
											<?php echo ( $excerpt != '' ) ? substr($excerpt, 0, 250) . ' [...]' : '' ?>
										</div>
									</div>
								</div>
							</div>
						</div>
                        -->
					</div>
					<?php $displayedOnSlideshow++; ?>
				<?php endwhile; ?>
			</div>
				<ol class="carousel-indicators">
					<?php for ($index = 0; $index < $displayedOnSlideshow; $index++): ?>
						<li data-target="#slideshow" data-slide-to="<?php echo $index ?>" class="<?php echo ($index === 0) ? 'active' : '' ?>"></li>
					<?php endfor; ?>
				</ol>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#slideshow" data-slide="prev"><i class="icon-chevron-left"></i></a>
				<a class="carousel-control right" href="#slideshow" data-slide="next"><i class="icon-chevron-right"></i></a>
		</div>
	</div>
	<?php define('DISPLAYEDONSLIDESHOW', $displayedOnSlideshow) ?>
<?php // endif; ?>