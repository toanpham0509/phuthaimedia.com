<?php
/**
 * Template file used to render an Author Archive Index page
 *
 * @subpackage  shprink_one
 * @since       1.0
 */
?>
<?php get_header(); ?>
<div class="container">
	<?php if (is_active_sidebar('before-content-widget')) : ?>
	<?php dynamic_sidebar('before-content-widget'); ?>
	<?php endif; ?>
	<!-- container start -->
	<div id="content">
		<div class="row">
			<?php shprinkone_get_sidebar('left'); ?>
			<div class="<?php echo shprinkone_get_contentspan(); ?>">
				<?php if (have_posts()) : ?>
				<?php the_post(); ?>
                <div class="page-header">
					<div class="icon-author">
						<?php echo "Đăng bởi <b><a href=''>"; printf(esc_attr__('%s ', 'shprinkone'), get_the_author()); echo "</a></b>"; ?>
						<small class="nickname"><i>(<?php the_author_meta('nickname'); ?>)
						</i></small>
					</div>
                </div>
				<?php rewind_posts(); ?>
				<?php endif; ?>
				<?php get_template_part('loop'); ?>
			</div>
			<?php shprinkone_get_sidebar('right'); ?>
		</div>
	</div>
	<?php if (is_active_sidebar('after-content-widget')) : ?>
	<?php dynamic_sidebar('after-content-widget'); ?>
	<?php endif; ?>
</div>
<!-- container end -->
<?php get_footer(); ?>