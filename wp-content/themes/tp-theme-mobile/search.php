<?php
/**
 * Template file used to render a Search Results Index page
 *
 * @subpackage  shprink_one
 * @since       1.0
 */
?>
<?php get_header(); ?>
<div class="container">
	<!-- container start -->
	<div id="content">
		<div class="row">
			<?php shprinkone_get_sidebar('left'); ?>
			<div class="<?php echo shprinkone_get_contentspan(); ?>">
				<section>
					<header class="page-header">
						<div class="icon-searchs">
							Kết quả tìm kiếm cho: <a href=""><b><?php printf(__(' %s', 'sprinkone'), get_search_query()); ?></b></a>
						</div>
					</header>
				</section>
				<?php get_template_part('loop'); ?>
			</div>
			<?php shprinkone_get_sidebar('right'); ?>
		</div>
	</div>
</div>
<!-- container end -->
<?php get_footer(); ?>