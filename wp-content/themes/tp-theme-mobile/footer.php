<?php
/**
 * Template footer
 *
 * @subpackage  shprink_one
 * @since       1.0
 */
$condition = is_active_sidebar('footer-widget-left') || is_active_sidebar('footer-widget-middle-left') || is_active_sidebar('footer-widget-middle-right') || is_active_sidebar('footer-widget-right');
$selectedTemplate = shprinkone_get_selected_template();
if ($selectedTemplate['value'] == 'cupid') {
	$copyright = __('All You Need Is %s | by %s', 'shprinkone');
	$copyrightIcon = '<i class="icon-heart"></i>';
} else {
	$copyright = __('%s Theme created by %s', 'shprinkone');
	$copyrightIcon = '<i class="icon-certificate icon-white"></i>';
}
?>
<?php if ($condition) : ?>
	<footer id="footer" class="well well-small">
		<div class="footer-inner">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-lg-3">
						<?php if (is_active_sidebar('footer-widget-left')) : ?>
							<?php dynamic_sidebar('footer-widget-left'); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-3 col-lg-3">
						<?php if (is_active_sidebar('footer-widget-middle-left')) : ?>
							<?php dynamic_sidebar('footer-widget-middle-left'); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-3 col-lg-3">
						<?php if (is_active_sidebar('footer-widget-middle-right')) : ?>
							<?php dynamic_sidebar('footer-widget-middle-right'); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-3 col-lg-3">
						<?php if (is_active_sidebar('footer-widget-right')) : ?>
							<?php dynamic_sidebar('footer-widget-right'); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</footer>
<?php endif; ?>
<section id="credit">
    <br />
    <div class='container'>
       <div class='row'>
           <div class='col-lg-8 col-md-8' style="text-align:left">
<span style="font-size:16px">Công ty cổ phần truyền thông Phú Thái. </span><br />
Trụ sở: P.805-N5C, KĐT Trung Hòa - Nhân Chính, Thanh Xuân, Hà Nội.<br />
VPGD: P.805-24T1, KĐT Trung Hòa - Nhân Chính, Cầu Giấy, Hà Nội.<br />
Tel : (84-4) 2215 4586<br />
Fax : (84-4) 6281 69111<br />
Email : info@phuthaimedia.com.vn<br />
           </div>
           <div class='col-lg-4 col-md-4' style="text-align:right">
                  <a href="<?php echo get_page_link('277')  ?>" style="margin-right:10px;" title="<?php echo get_the_title(277)  ?>">
		<?php echo get_the_title(277); ?>
	</a> |
    <a href="<?php echo get_page_link('284')  ?>" style="margin-left:10px;" title="<?php echo get_the_title(284)  ?>">
		<?php echo get_the_title(284); ?>
	</a>
           </div>
       </div>
   </div>  
   <div id="googleMap" style="width:95%; height:150px;margin-top:10px;margin:auto;margin-top:10px;"></div>
	<div class="credit-inner">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<div class="pull-left">
						&copy;
						<?php echo date("Y"); ?>
						<?php bloginfo('name'); ?>
						<?php _e('All rights reserved.', 'shprinkone') ?>
						| Powered by <a href="http://kool-soft.com/">Kool-soft.com</a>
					</div>
					<?php if (is_active_sidebar('footer-widget-bottom')) : ?>
						<?php dynamic_sidebar('footer-widget-bottom'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php wp_footer(); ?>
</body>
</html>
