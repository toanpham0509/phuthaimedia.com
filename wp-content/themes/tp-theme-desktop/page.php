<?php get_header(); ?>
<?php if( function_exists("the_breadcrumbs") ) the_breadcrumbs(); ?>
<br />
<div id="content" style="background-color:#FFF;padding:10px 10px;">
	<div class="row">
	    	<div class="col-lg-12" style="overflow:hidden;">
        	<?php if( have_posts() ): ?>
            	<?php while( have_posts() ): ?>
                	<?php the_post(); ?>
                	<article id="post-<?php the_ID() ?>" <?php post_class() ?>  style="position:relative" >
			<!--
                    	<div class="icon-article">
                        	<div class="content">
    		                    	<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
            	        </div>
                        <hr />
			-->
			<br />	
						<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>	
							<?php the_tags( __('<hr /><div class="icon-tagss"> ','html5reset'), ', ', '<br /></div>'); ?>
						</div>
                        <hr />
                    </article>
                <?php endwhile; ?>
            <?php endif; ?>
			<?php // comments_template(); ?>
    	    </div>
    </div>
</div>    
<?php get_footer(); ?>