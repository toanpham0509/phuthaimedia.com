<div class="sidebar-right">
                <h2 class="box-title">Báo giá</h2>
                <ul class="item-quote">
        		<?php $args = array(
						  'cat'         => '28',
						  'orderby'          => 'post_date',
						  'order'            => 'DESC',
						  'post_status'      => 'publish'
					  );
					  $myPosts = new WP_Query( $args );
					  while( $myPosts->have_posts() ) {
						  $myPosts->the_post();
						  ?>
                          <li>
                          	 <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                          		<?php 
									if( !has_post_thumbnail() ) {
										?>
                                        <img src="<?php bloginfo("template_url");?>/images/no_image.png" class="img-responsive" />
                                        <?php
									} else {
										the_post_thumbnail( "full" );
									}
									the_title();
								?>
                             </a>
                          </li>
                          <div class="clear"></div>
                          <?php
					  }
				?>
                </ul>
                </div>