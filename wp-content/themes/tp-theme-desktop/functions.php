<?php
	include_once("functions/Walker/wp_bootstrap_navwalker.php");
	include_once("functions/Walker/Comment.php");
	
	//
	add_theme_support("post-thumbnails");
	add_theme_support( 'automatic-feed-links' );
	
	//Register menus.
	register_nav_menu( 'menu-top', __( 'Main Menu - Top menu', 'html5reset' ) );
	register_nav_menu( 'menu-bottom', __( 'Bottom menu - Bottom menu', 'html5reset' ) );
	
	
	//Insert js
	function tp_insert_javascript() {
		//Library
		wp_enqueue_script("library", get_stylesheet_directory_uri() . '/js/jquery-2.1.0.min.js' );
		//Tempalte js
		wp_enqueue_script("templateJs", get_stylesheet_directory_uri() . '/js/template.min.js' );
		//Style
	    wp_enqueue_script("style", get_stylesheet_directory_uri() . '/js/style.js' );
		//to-top
		wp_enqueue_script("to-top", get_stylesheet_directory_uri() . '/js/to-top.js' );
		//js for menu.
		wp_enqueue_script("menu", get_stylesheet_directory_uri() . '/js/menu.js' );
		
		//Js for slider.
		wp_enqueue_script("slider cycle", get_stylesheet_directory_uri() . '/js/jquery.cycle2.js' );
		wp_enqueue_script("slider cycle1", get_stylesheet_directory_uri() . '/js/jquery.cycle2.carousel.js' );
		
		//Js để nhận dạng thiết bị.
		wp_enqueue_script("Device", get_stylesheet_directory_uri() . '/js/device.js' );
	}
	add_action( 'wp_enqueue_scripts', 'tp_insert_javascript' );
	
	//Breadcrumb.
	function the_breadcrumbs() {
		echo "<br />";
		echo "<div id=\"content\">";
    	$delimiter = '»';
  		$home = 'Trang chủ';
  		$before = '<span>'; // thẻ html đằng trước mỗi link
 		$after = '</span>'; // thẻ đằng sau mỗi link
 
		if ( !is_home() && !is_front_page() || is_paged() ) {
	    	echo '<div class="breadcrumb" style="margin-bottom:0px;">';
			global $post;
 			$homeLink = get_bloginfo('url');
 			echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
			if ( is_category() ) {
 				global $wp_query;
 				$cat_obj = $wp_query->get_queried_object();
 				$thisCat = $cat_obj->term_id;
 				$thisCat = get_category($thisCat);
 				$parentCat = get_category($thisCat->parent);
 				if ($thisCat->parent != 0) echo( get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ') );
 				echo $before . single_cat_title('', false) . $after;
 
 			} elseif ( is_day() ) {
 				echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 				echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
 				echo $before . get_the_time('d') . $after;
 			} elseif ( is_month() ) {
 				echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
 				echo $before . get_the_time('F') . $after;
 			} elseif ( is_year() ) {
 				echo $before . get_the_time('Y') . $after;
 			} elseif ( is_single() && !is_attachment() ) {
 				if ( get_post_type() != 'post' ) {
 					$post_type = get_post_type_object(get_post_type());
 					$slug = $post_type->rewrite;
 					echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
 					echo $before . get_the_title() . $after;
 				} else {
 					$cat = get_the_category(); 
					$cat = $cat[0];
 					echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 					echo $before . get_the_title() . $after;
 				}
 			} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
 				$post_type = get_post_type_object(get_post_type());
 				echo $before . $post_type->labels->singular_name . $after;
 			} elseif ( is_attachment() ) {
 				$parent = get_post($post->post_parent);
 				$cat = get_the_category($parent->ID); $cat = $cat[0];
 				echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
 				echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
 				echo $before . get_the_title() . $after;
 			} elseif ( is_page() && !$post->post_parent ) {
 				echo $before . get_the_title() . $after;
 			} elseif ( is_page() && $post->post_parent ) {
				$parent_id = $post->post_parent;
 				$breadcrumbs = array();
 				while ($parent_id) {
 					$page = get_page($parent_id);
 					$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
 					$parent_id = $page->post_parent;
 				}
 				$breadcrumbs = array_reverse($breadcrumbs);
 				foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
 				echo $before . get_the_title() . $after;
 			} elseif ( is_search() ) {
 				echo $before . 'Kết quả tìm kiếm cho "' . get_search_query() . '"' . $after;
 			} elseif ( is_tag() ) {
 				echo $before . 'Bài viết có tag là "' . single_tag_title('', false) . '"' . $after;
 			} elseif ( is_author() ) {
 				global $author;
 				$userdata = get_userdata($author);
 				echo $before . 'Bài viết được đăng bởi ' . $userdata->display_name . $after;
 			} elseif ( is_404() ) {
 				echo $before . 'Error 404' . $after;
			}
			if ( get_query_var('paged') ) {
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() )
					echo ' (';
				echo __('Page') . ' ' . get_query_var('paged');
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) 
					echo ')';
			}
		echo "</div>";
		echo "</div>";
  		}
	}
	
	//Get post meta
	function get_post_meta_( $inline = false, $author = true, $date = false, $category = true, $tag = true, $comments = false, $sticky = false, $tooltip = false){
		$inline = ($inline) ? 'list-inline' : 'list-unstyled';
		$html = '<div class = "post-meta">';
		$html .= '<ul class = "' . $inline . '">';
		if (is_sticky() && $sticky) {
			$html .= '<li class = "post-sticky label label-info" ';
			$html .= ($tooltip) ? 'data-content="' . __('Featured', 'shprinkone') . '"' : '';
			$html .= '><i class = "icon-star"></i> ';
			$html .= ($tooltip) ? '' : __('Featured', 'shprinkone');
			$html .= '</li>';
		}
		if ($date) {
			$formatedDate = get_the_date(__('M d, Y', 'shprinkone'));
			$html .= '<li class = "post-date" ';
			$html .= ($tooltip) ? 'title="' . __('Ngày xuất bản', 'shprinkone') . '" data-content="' . $formatedDate . '"' : '';
			$html .= '><i class = "icon-calendar"></i> ';
			$html .= ($tooltip) ? '' : $formatedDate;
			$html .= '</li>';
		}
		if ($author) {
			$authorName = get_the_author();
			$html .= '<li class = "post-author" ';
			$html .= ($tooltip) ? 'title="' . __('Tác giả', 'shprinkone') . '" data-content="' . $authorName . '"' : '';
			$html .= '><i class = "icon-user"></i> ';
			$html .= ($tooltip) ? '' : shprinkone_get_the_author_posts_link();
			$html .= '</li>';
		}
		if ($category) {
			if (has_category()):
				$categories = get_the_category();
				$categoryList = array();
				foreach ($categories as $category) {
					$categoryList[] = $category->cat_name;
				}
				$html .= '<li class = "post-category" ';
				$html .= ($tooltip) ? 'title="' . __('Chuyên mục', 'shprinkone') . '" data-content="' . join(", ", $categoryList) . '"' : '';
				$html .= '><i class = "icon-folder-open"></i> ';
				$html .= ($tooltip) ? '' : sprintf(__('Chuyên mục: %s', 'shprinkone'), get_the_category_list(', ', '', false));
				$html .= '</li>';
			endif;
		}
		if ($tag) {
			if (has_tag()):
				$tags = get_the_tags();
				$tagList = array();
				foreach ($tags as $tag) {
					$tagList[] = $tag->name;
				}
				$html .= '<li class = "post-tags" ';
				$html .= ($tooltip) ? 'title="' . __('Từ khóa ', 'shprinkone') . '" data-content="' . join(", ", $tagList) . '"' : '';
				$html .= '><i class = "icon-tags"></i> ';
				$html .= ($tooltip) ? '' : get_the_tag_list(__('Từ khóa  ', 'shprinkone'), ' ');
				$html .= '</li>';
			endif;
		}
		if ($comments) {
			$commentNumber = get_comments_number();
			$html .= '<li class = "post-comments" ';
			$html .= ($tooltip) ? 'title="' . __('Bình luận', 'shprinkone') . '" data-content="' . $commentNumber . '"' : '';
			$html .= '><i class = "icon-comment"></i> ';
			$html .= $commentNumber;
			$html .= '</li>';
		}
		$html .= '</ul>';
		$html .= '</div>';
		return $html;
	}
	
	//Custom excerpt for post.
	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	function new_excerpt_more( $excerpt ) {
		return str_replace( '[...]', '...', $excerpt );
	}
	add_filter( 'wp_trim_excerpt', 'new_excerpt_more' );
	
	
	//lấy ra excerpt.
	function tp_the_excerpt($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;
		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
		echo '<a class="readmore" href="'. get_permalink( get_the_ID() ) . '">';
		if( get_bloginfo("language") == "en" ){
			echo "Read more";
		}else {
			echo "Đọc thêm";
		}
		echo '</a>';
		return $q;
	}

	function tp_get_post_in_cat_for_slider($category_id, $showposts = 12){
		$active = "true";
		$item_count = 0;
		$limit_post_on_a_slide_item = 3;
		$args = array (
			"cat" => $category_id,
			"post_status" => "publish",
			"showposts" => $showposts
		);
		$the_query = new WP_Query($args);
		if( $the_query->have_posts() ): 
			while( $the_query->have_posts() ):
				$the_query->the_post();
				
				if( $item_count % $limit_post_on_a_slide_item == 0 ):
					if( $active == "true" ) :
						$active = "false";
						echo "<div class='item active'>";
						echo "<div class='row'>";	
					else :
						echo "<div class='item'>";
						echo "<div class='row'>";	
					endif;
				endif;
				
				echo '<div class="item-news">';//start div.item-news
				?>
						<a href='<?php the_permalink() ?>' title='<?php the_title(); ?>'>
                            <?php if( has_post_thumbnail() ): ?>
                            	<?php // the_post_thumbnail(); ?>
                                <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 315, 200) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                            <?php else : ?>
                            	<img src="<?php bloginfo("template_url"); ?>/images/no_image.png" />
                            <?php endif; ?>
						</a>
					
					<div class="title">
						<h2 style="font-size:18px;margin-top:10px;margin-bottom:10px;"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					</div>
					<div class="excerpt">
						<?php tp_the_excerpt(200); ?>   	 
					</div>
				<?php
				echo '</div>';	//end div.item-news.
				
				if( $item_count % $limit_post_on_a_slide_item == 2 ):
					echo "</div>";	//end div.row
					echo "</div>";	//end div.item
				endif;
				$item_count ++;
			endwhile;	//End while.
			if( ( $item_count - 1 ) % $limit_post_on_a_slide_item != 2) : ?>
				</div><!--end row-->
				</div><!--end item-->
			 <?
			 endif;
		endif;	//End if.
	}
	function get_posted_on(){
		printf(__( 'Đăng lúc', 'bonestheme' )
		   . ' <time class="updated" datetime="%1$s" pubdate><a href="#" title="">%2$s</a></time> '
		   . __('bởi', 'bonestheme' ) 
		   . ' <span class="author"><a href=" '. home_url() . "/author/" .'%3$s">%3$s</a></span> <span class="amp"> và</span> ' 
		   . __('thuộc chuyên mục', 'bonestheme') .  ' %4$s.', get_the_time('Y-m-j'), get_the_time(__( ' g:i A Y/m/d ', 'bonestheme' )), get_the_author_link( get_the_author_meta( 'ID' ) ), get_the_category_list(', '));
	}
	
   /*
	* Resize image using timthumb
	*	
	*/
	function gth_resize_img($src, $width, $height, $zc='1', $quality='90') {
		  $img_url = esc_url( get_template_directory_uri() . '/functions/thumb.php?src=' . $src . '&amp;w=' . $width . '&amp;h=' . $height . '&amp;zc=' . $zc . '&amp;q=' . $quality);
		  return $img_url;
	}
	
	function gth_post_thumbnail() {
    if (has_post_thumbnail()) {
        $image_id = get_post_thumbnail_id( "full" );
        $image_url = wp_get_attachment_image_src($image_id, $size);
        $image_url = $image_url[0];
    } else {
        global $post, $posts;
        $image_url = '';
        ob_start();
        ob_end_clean();
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        $image_url = $matches [1] [0];

        if (empty($image_url)) {
            $embed = esc_html( get_post_meta( $post->ID, 'embed', true ) );
            $image_url = esc_url( woo_get_video_image( $embed ) );
        }
        
        //Defines a default image
        if(empty($image_url)) {
            $image_url = get_bloginfo('template_url') . "/images/default.jpg";
        }
    }
    	return $image_url;
	}
	
	//Get cat id.
	function tp_get_category_id($cat_name){
		$term = get_term_by('name', $cat_name, 'category');
		return $term->term_id;
	}