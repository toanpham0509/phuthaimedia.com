<!--
<div id="footer">
	<div id="content">
    	<br />
        <div class="row" style="color:#CCC;width:80%">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            	<h1 style="margin:0;"><p style="font-size:16px;"><b><?php echo get_cat_name(7); ?></b></p></h1>
            	<?php
                	$args = array(
						"parent" => 7
					);
					$my_cats = get_categories($args);
					echo "<ul class='ul'>";
						foreach($my_cats as $my_cat){
							?>
                            <li><h2><a href="<?php echo get_category_link($my_cat->id); ?>" title="<?php $my_cat->name ?>"><?php echo $my_cat->name ?></a></h2></li>
                            <?php
						}
					echo "</ul>";
				?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            	<h1 style="margin:0;"><p style="font-size:16px;"><b><?php echo get_cat_name(4); ?></b></p></h1>
            	<?php
                	$args = array(
						'category'         => '4',
						'post_status' => 'publish',
						'posts_per_page'   => 5,
						'orderby'          => 'post_date',
						'order'            => 'DESC',
						'post_type'        => 'post'
						
					);
					$my_cats = get_posts($args);
					echo "<ul class='ul'>";
						foreach($my_cats as $my_cat){
							?>
                            <li><h2><a href="<?php echo get_permalink($my_cat->ID); ?>" title="<?php $my_cat->post_title ?>">
								<?php echo $my_cat->post_title ?>
                            </a></h2></li>
                            <?php
						}
					echo "</ul>";
				?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            	<h1 style="margin:0;"><p style="font-size:16px;"><b><?php echo get_cat_name(8); ?></b></p></h1>
            	<?php
                	$args = array(
						'category'         => '8',
						'post_status' => 'publish',
						'posts_per_page'   => 5,
						'orderby'          => 'post_date',
						'order'            => 'DESC',
						'post_type'        => 'post'
						
					);
					$my_cats = get_posts($args);
					echo "<ul class='ul'>";
						foreach($my_cats as $my_cat){
							?>
                            <li><h2><a href="<?php echo get_permalink($my_cat->ID); ?>" title="<?php $my_cat->post_title ?>">
								<?php echo $my_cat->post_title ?>
                            </a></h2></li>
                            <?php
						}
					echo "</ul>";
				?>
            </div>
        </div>
        
    	<div class="footer-link" style="text-align:right;float:right;position:absolute;right:0px;top:0px;">
            <br />
            <?php
				wp_nav_menu( array(
					'menu'              => 'menu-bottom',
					'theme_location'    => 'menu-bottom',
					'depth'             => 1,
					'container'         => false,
					'container_class'   => '',
					'container_id'      => '',
					'menu_class'        => 'menu',
					'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					'walker'            => new wp_bootstrap_navwalker())
				);
			?>
            <div id="googleMap" style="width:250px; height:150px;float:right; margin-top:10px;"></div>
        </div>
        <br /><br />
        <div class="row">
	        <div class="col-lg-6 col-md-6 col-sm-6 col-xs6" style="color:#CCC">
        		<h1 style="margin:0;"><p style="font-size:16px;"><b>Công ty cổ phần truyền thông Phú Thái</b></p></h1>
	            <h2 style="font-size:13px;margin:0;">
    	        Trụ sở: P.805-N5C, KDT Trung Hòa - Nhân Chính, Thanh Xuân, Hòa Nội. <br />
        	    VPGD: P.805-24T1, KDT Trung Hòa - Nhân Chính, Cầu Giấy, Hà Nội. <br />
            	Tel		: (84-4) 2215 4586 <br />
	            Fax     : (84-4) 6281 69111 <br />
    	        Email   :  info@phuthaimedia.com.vn<br />
        	    </h2>
            </div>
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs6" style="position:relative;">
            	<div class="copy-right">
		        	Phuthai media 2009 copyright @ <br >
        		    All right reverved. Industry-leading Antivirus Software Sitemap | Privacy policy.
            	</div>
        	</div>
        </div>
    </div><br />
    <!--
    <div class="footer-menu">
	    <?php
			wp_nav_menu( array(
				'menu'              => 'menu-top',
				'theme_location'    => 'menu-top',
				'depth'             => 1,
				'container'         => false,
				'container_class'   => '',
				'container_id'      => '',
				'menu_class'        => '',
				'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				'walker'            => new wp_bootstrap_navwalker())
			);
		?>
    </div>
    -->
    <!--
    <div id="to-top">
    	<a href="#top" title="Lên đầu trang">
        	<img src="<?php bloginfo("template_url"); ?>/images/to top.jpg" />
        </a>
    </div>
    -->
    <!--
</div>
<?php wp_footer() ?>
</body>
</html>
-->