<div class="sidebar-right">
    <h2 class="box-title"><?php echo get_page(524)->post_title ?></h2>
	<iframe style="margin-top:0px;" width="100%" height="200" src="<?php echo strip_tags( get_post(524)->post_content ); ?>" frameborder="0" allowfullscreen>
    </iframe>
	<hr />
<?php
	if( is_single() && !in_category( 58 ) ){
		?>
        <h4 class="text-success box-title">
	    	Bài viết cùng chuyên mục
    	</h4>
        <?php
			$cat = get_the_category(); 
			$cat_id = $cat[0]->term_id;
			$args = array (
				"cat"=>$cat_id,
				"post_status" => "publish"
			);
			$the_query = new WP_Query($args);
			if( $the_query->have_posts() ) {
				?>
                <marquee height="200px" behavior="scroll" direction="up" scrollamount="2" width="100%;" onmousemove="this.stop()" onmouseout="this.start()">
                <?php
				echo "<ul class='list'>";
				while( $the_query->have_posts() ):
					$the_query->the_post();
					echo "<li>";
						echo "<a href='";
							the_permalink();
						echo "'>";
						if( has_post_thumbnail() ) {
							?>
							<img style="float:left;width:80px;height:60px;margin-right:10px; margin-top: -10px;" src="<?php echo gth_resize_img(gth_post_thumbnail(), 250, 150) ?>" class="img-responsive" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                            <?
						} else {
							?>
                            <img style="float:left;width:80px;height:60px;margin-right:10px; margin-top: -10px;" src="<?php bloginfo("template_url"); ?>/images/no_image.png" class="img-responsive" />
                            <?php
						}
							the_title();
						echo "</a>";
						echo "<div class='clearfix'></div>";
					echo "</li>";
				endwhile;
				echo "</ul>";
				?>
                </marquee>
                <?php
			}
	}
?>
	<?php wp_reset_query(); ?>
	<?php get_sidebar('part-latest-news'); ?>
</div>