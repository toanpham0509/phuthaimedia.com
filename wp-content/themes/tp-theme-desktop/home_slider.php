<div id="content" style="margin-top:10px">
    <div class="slider">
    	<div id="main-slider" class="carousel slide" data-ride="carousel">
  			
			<!-- Wrapper for slides -->
  			<div class="carousel-inner">
            	<?php  
					$count = 0;
					$feat_id = 16;
					$limit = 5;
					$active = true;
					$tags = "home-slide";
					//$the_query = new WP_Query( 'cat=' . $feat_id . '&showposts=' . $limit );
					$the_query = new WP_Query( 'tag=' . $tags . '&showposts=' . $limit );
					 while ( $the_query->have_posts()) : $the_query->the_post();
						  if ($active) { ?>
						  <div class="item active">
						  <?php $active = false; } else { ?>
						  <div class="item">
						  <?php } ?>      
                                                  <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
							   <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 1100, 350) ?>" height="350" width="1100" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                               <div class="carousel-caption">
                                                  </a> 
					    	    	<?php // the_title(); ?>
							   	</div>
						  </div>
                          <?php $count++; ?>
				<?php endwhile; ?>
                <?php wp_reset_query(); ?>
			</div>
            
            <!-- Controls -->
  			<a class="left carousel-control" href="#main-slider" role="button" data-slide="prev">
    			<span class="glyphicon glyphicon-chevron-left"></span>
    			<span class="sr-only">Previous</span>
  			</a>
  			<a class="right carousel-control" href="#main-slider" role="button" data-slide="next">
    			<span class="glyphicon glyphicon-chevron-right"></span>
    			<span class="sr-only">Next</span>
  			</a>
			
            <!-- Indicators -->
  			<ol class="carousel-indicators">
            	<?php  
					for($i = 0; $i < $count; $i++){
						?>
                        <li data-target="#main-slider" data-slide-to="<?php echo $i; ?>" <?php if($i == 0) echo " class = 'active' " ?>></li>
                        <?php
					}
				?>
  			</ol>
            
            <div class="slider-shadow"></div>
		</div>
    </div>
</div>
    <div class="clear"></div>