<form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo("siteurl"); ?>">
    <div>
        <label class="screen-reader-text label brown" for="s">Nhập vào từ khóa để tìm kiếm:</label>
        <br /><br />
        <div class="row">
        	<div class="col-lg-6"><input class="form-control" type="text" value="" name="s" id="s"></div>
	        <div class="col-lg-3"><input type="submit" class="form-control" id="searchsubmit" value="Tìm kiếm"></div>
        </div>
    </div>
</form>