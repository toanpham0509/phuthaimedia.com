<?php
get_header();
	if( function_exists("the_breadcrumbs") ) the_breadcrumbs();
	?>
	<br />
	<div id="content" style="background-color:#FFF;padding:10px 10px;">
		<div class="row">
	    	<div class="col-lg-8" style="overflow:hidden; padding-left: 30px;">
		        <div class="box-new">
			    	<div class="body" style="padding:0px 0px;margin-left:-20px;">
					<?php 
           				if( have_posts() ) :
							while( have_posts() ):
								the_post();
								echo "<div class='item-news active-1' style='margin-right:5px;margin-top:0px;margin-bottom:15px;width:330px;'>";
										?>
										<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
										<?php 
											if( has_post_thumbnail() ){
												the_post_thumbnail();
											}else { ?>
												<img src="<?php bloginfo("template_url"); ?>/images/no_image.png" />
											<?php } ?>
										</a>
										<?php
									echo "<div class='title'>";
										echo "<h2 style='font-size:18px;margin-top:10px;'>";
										echo "<a href='";
											the_permalink();
										echo "'>";
											the_title();
										echo "</a>";
										echo "</h2>";
									echo "</div>";
									echo "<div class='excerpt'>";
										tp_the_excerpt(100);
									echo "</div>";
									?>
									<?php
								echo "</div>";
							endwhile;
						endif;
						wp_reset_query();
        			?>
        			</div>
                </div>
		    </div>
    		<div class="col-lg-4">
            	<?php get_sidebar( "bao-gia" ); ?>
		    </div>
        </div>
    </div>
    <?php
get_footer();