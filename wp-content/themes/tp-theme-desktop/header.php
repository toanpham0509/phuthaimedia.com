<!DOCTYPE html <?php language_attributes(); ?> PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo("html_type") ?>" charset="<?php bloginfo("charset") ?>" />
    <meta name="keywords" content="<?php bloginfo("name") ?> | <?php bloginfo("description") ?>"  />
    <meta name="version" content="<?php bloginfo("version") ?>" />
    <meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">
    <meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
    <?php
		//Robots
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
		else
			echo '<meta name="robots" content="index, follow" />';
	?>
    <?php
	if( function_exists("of_get_option") ) {
		//Author.
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>
    <?php
		//Meta_viewport.
		if (true == of_get_option('meta_viewport'))
			echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . ' minimal-ui" />';
		
		//favicon.
		if (true == of_get_option('head_favicon')) {
			echo '<meta name=”mobile-web-app-capable” content=”yes”>';
			echo '<link rel="shortcut icon" sizes=”1024x1024” href="' . of_get_option("head_favicon") . '" />';
		}
		//Apple touch icon.
		if (true == of_get_option('head_apple_touch_icon'))
			echo '<link rel="apple-touch-icon" href="' . of_get_option("head_apple_touch_icon") . '">';	
	?>
    
    <!-- Application-specific meta tags -->
	<?php
		// Windows 8
		if (true == of_get_option('meta_app_win_name')) {
			echo '<meta name="application-name" content="' . of_get_option("meta_app_win_name") . '" /> ';
			echo '<meta name="msapplication-TileColor" content="' . of_get_option("meta_app_win_color") . '" /> ';
			echo '<meta name="msapplication-TileImage" content="' . of_get_option("meta_app_win_image") . '" />';
		}

		// Twitter
		if (true == of_get_option('meta_app_twt_card')) {
			echo '<meta name="twitter:card" content="' . of_get_option("meta_app_twt_card") . '" />';
			echo '<meta name="twitter:site" content="' . of_get_option("meta_app_twt_site") . '" />';
			echo '<meta name="twitter:title" content="' . of_get_option("meta_app_twt_title") . '">';
			echo '<meta name="twitter:description" content="' . of_get_option("meta_app_twt_description") . '" />';
			echo '<meta name="twitter:url" content="' . of_get_option("meta_app_twt_url") . '" />';
		}

		// Facebook
		if (true == of_get_option('meta_app_fb_title')) {
			echo '<meta property="og:title" content="' . of_get_option("meta_app_fb_title") . '" />';
			echo '<meta property="og:description" content="' . of_get_option("meta_app_fb_description") . '" />';
			echo '<meta property="og:url" content="' . of_get_option("meta_app_fb_url") . '" />';
			echo '<meta property="og:image" content="' . of_get_option("meta_app_fb_image") . '" />';
		}
	}
	?>
    
    <?php //reponsive ?>
	<meta content="IE=edge" http-equiv="X-UA-Compatible" /><!-- for IE -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> <!-- to Reposive -->
    
    <link href="<?php bloginfo("template_url"); ?>/css/template.min.css" media="screen" rel="stylesheet" />
    <link href="<?php bloginfo("template_url"); ?>/css/template-theme.min.css" media="screen" rel="stylesheet" />
    <link href="<?php bloginfo("template_url"); ?>/css/style.css" media="screen" rel="stylesheet" />
    <link href="<?php bloginfo("template_url"); ?>/css/to-top.css" media="screen" rel="stylesheet" />
    
	<?php // icons & favicons ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/icon.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/icon.png">
    <!--[if IE]>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/icon.png">
    <![endif]-->
    <?php // or, set /favicon.ico for IE10 win ?>
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/icon.png">
 
    <link rel="shortcut icon" href="<?php bloginfo("template_url") ?>/images/icon.png" />
    
    <?php //Pingback XML ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    
    <?php // các file js được chèn thông qua file functions.php ?>
    
    <?php wp_head(); ?>
    
    <?php //googleMap 
    ?>
    	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
    	<script>
    		var myCenter=new google.maps.LatLng(21.00828,105.802027);

    		function initialize()
    		{
	    		var mapProp = {
	     		 center:myCenter,
	      		zoom:18,
    	  		mapTypeId:google.maps.MapTypeId.ROADMAP
	      		};

    			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

	    		var marker=new google.maps.Marker({
	     		 position:myCenter,
    	 		 animation:google.maps.Animation.BOUNCE
	      		});

    			marker.setMap(map);

	   		var infowindow = new google.maps.InfoWindow({
	      		content:"PhuThai media!"
    		  });

	    	infowindow.open(map,marker);
	    }

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
 
    <?php if( !is_user_logged_in() ) { ?>
    	<style type="text/css" media="screen">
			.qtrans_flag span { display:none }
			.qtrans_flag { height:12px; width:18px; display:block }
			.qtrans_flag_and_text { padding-left:20px }
			.qtrans_flag_vi { background:url(http://localhost/phuthaimedia/wp-content/plugins/qtranslate/flags/vn.png) no-repeat }
			.qtrans_flag_en { background:url(http://localhost/phuthaimedia/wp-content/plugins/qtranslate/flags/gb.png) no-repeat }
		</style>
		<link hreflang="en" href="http://localhost/phuthaimedia/?lang=en" rel="stylesheet" />
			<style type="text/css" media="print">#wpadminbar { display:none; }</style>
			<style type="text/css" media="screen">
			html { margin-top: 0px !important; }
			* html body { margin-top: 0px !important; }
			@media screen and ( max-width: 782px ) {
				html { margin-top: 0px !important; }
				* html body { margin-top: 0px !important; }
			}
		</style>
    <?php } ?>
    <title>
		<?php 
			if( !is_home() ) {
				wp_title("|", true, "right");
			} else { 
				bloginfo("name");
				echo " | ";
				bloginfo("description");
			}
		?>
    </title>
</head>
<body <?php body_class(); ?> onLoad="initialize()" >
<div id="header">
	<div class="container-fluid" <?php echo 'style="background-color:#FFF !important"';  ?>>
       	<div id="content">
        <form action="<?php echo bloginfo("siteurl"); ?>" method="get" role="search" >
            <div class="search-language">
                <a class="icon-language" href="<?php echo home_url() ?>/?lang=vi" title="Ngôn ngữ Tiếng việt"><img src="<?php bloginfo("template_url"); ?>/images/VN.png" width="26px" height="26px" /></a>
                <a class="icon-language" href="<?php echo home_url() ?>/?lang=en" title="English language"><img src="<?php bloginfo("template_url"); ?>/images/EN.png" width="24px" height="24px" style="margin-top:1px;" /></a>
                <input type="search" name="s" class="" placeholder="Tìm kiếm..." />
            </div>
		</form>
        </div>
        <div id="menu">
            <nav class="navbar" role="navigation">
			      <div id="content" style="overflow:visible">
			        <div class="navbar-header">
				        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				        </button>
                        <div class="logo">
				            <a href="<?php echo home_url() ?>" title="<?php bloginfo("name"); ?>">
               					<img src="<?php bloginfo("template_url") ?>/images/logo.png" />
				            </a>
				        </div>
        			</div>
                    
    		    	<div id="navbar" class="collapse navbar-collapse menu">
			             <?php
        		        	wp_nav_menu( array(
                		  		'menu'              => 'menu-top',
		                  		'theme_location'    => 'menu-top',
		                  		'depth'             => 2,
		                  		'container'         => false,
		                  		'container_class'   => '',
		                  		'container_id'      => '',
		                  		'menu_class'        => 'navbar-nav',
		                  		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                  		'walker'            => new wp_bootstrap_navwalker())
        		       		);
		            	?>
        			</div><!--/.nav-collapse -->
			      </div>
    		 </nav>
          </div>
       </div>
</div>
<?php if( is_home() ): get_template_part("home_slider"); ?>
<?php endif; ?>
<?php wp_reset_query(); ?>