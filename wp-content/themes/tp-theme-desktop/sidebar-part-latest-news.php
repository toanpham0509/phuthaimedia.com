<?php
	//Sidebar part latest news	
?>
	<hr />
	<h2 class="box-title">Tin mới</h2>
    <?php
    	$args = array(
			"showposts" => 10,
			"order_by" => "date",
			"order" => "asc"
		);
		$the_query = new WP_Query($args);
			if( $the_query->have_posts() ) {
				?>
                <marquee behavior="scroll" direction="up" scrollamount="2" width="100%" style="height:200px;position:relative;" onmousemove="this.stop()" onmouseout="this.start()">
                <?php
				echo "<ul class='list'>";
				while( $the_query->have_posts() ):
					$the_query->the_post();
					echo "<li>";
						echo "<a href='";
							the_permalink();
						echo "'>";
						if( has_post_thumbnail() ) {
							?>
							<img style="float:left;width:80px;height:60px;margin-right:10px; margin-top: -10px;" src="<?php echo gth_resize_img(gth_post_thumbnail(), 250, 150) ?>" class="img-responsive" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                            <?
						} else {
							?>
                            <img style="float:left;width:80px;height:60px;margin-right:10px; margin-top: -10px;" src="<?php bloginfo("template_url"); ?>/images/no_image.png" class="img-responsive" />
                            <?php
						}
							the_title();
						echo "</a>";
						echo "<div class='clearfix'></div>";
					echo "</li>";
				endwhile;
				echo "</ul>";
				?>
                </marquee>
                <?php
			}
	?>