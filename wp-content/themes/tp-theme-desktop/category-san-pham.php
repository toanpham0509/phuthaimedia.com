<?php get_header(); ?>
<div id="content">
<br />
    <div class="box-new">
        <div class="header">
      	   <div class="title">
           		<h4 class="title">
               <a href="<?php echo get_category_link(11); ?>" title="<?php echo get_cat_name(11); ?>" style="margin-left:-10px">
					<?php echo get_cat_name(11); ?>
               </a>
               </h4>
           </div>
           <a href="<?php echo get_category_link(11) ?>"><div class="background-right-box-news"></div></a>
        </div>
        <div class="body">
                <div id="slider-2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <?php
						tp_get_post_in_cat_for_slider(11, 9);
					?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-2" role="button" data-slide="prev">
                    	<img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-2" role="button" data-slide="next">
                    	<img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
        </div>
	</div>
    
    <div class="box-new">
        <div class="header">
      	   <div class="title">
           		<h4 class="title">
               <a href="<?php echo get_category_link(13); ?>" title="<?php echo get_cat_name(13); ?>" style="margin-left:-10px">
					<?php echo get_cat_name(13); ?>
               </a>
               </h4>
           </div>
            <a href="<?php echo get_category_link(13) ?>"><div class="background-right-box-news"></div></a>
        </div>
        <div class="body">
                <div id="slider-3" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <?php
						tp_get_post_in_cat_for_slider(13, 9);
					?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-3" role="button" data-slide="prev">
                    	<img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-3" role="button" data-slide="next">
                    	<img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
        </div>
	</div>
    
    <div class="box-new">
        <div class="header">
      	   <div class="title">
           		<h4 class="title">
               <a href="<?php echo get_category_link(10); ?>" title="<?php echo get_cat_name(10); ?>" style="margin-left:-10px">
					<?php echo get_cat_name(10); ?>
               </a>
               </h4>
           </div>
           <a href="<?php echo get_category_link(10) ?>"><div class="background-right-box-news"></div></a>
        </div>
        <div class="body">
                <div id="slider-1" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <?php
						tp_get_post_in_cat_for_slider(10, 9);
					?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-1" role="button" data-slide="prev">
                    	<img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-1" role="button" data-slide="next">
                    	<img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
        </div>
	</div>
    <div class="box-new">
        <div class="header">
      	   <div class="title">
           	   <h4 class="title">
               <a href="<?php echo get_category_link(27); ?>" title="<?php echo get_cat_name(27); ?>" style="margin-left:-10px">
					<?php echo get_cat_name(27); ?>
               </a>
               </h4>
           </div>
           <a href="<?php echo get_category_link(27) ?>"><div class="background-right-box-news"></div></a>
        </div>
        <div class="body">
                <div id="slider-4" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <?php
						tp_get_post_in_cat_for_slider(27, 9);
					?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-4" role="button" data-slide="prev">
                    	<img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-4" role="button" data-slide="next">
                    	<img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
        </div>
	</div>
</div>
<?php get_footer(); ?>