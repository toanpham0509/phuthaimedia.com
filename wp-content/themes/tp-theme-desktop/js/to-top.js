// JavaScript Document
$(document).ready(function(e) {
	/*js for to top*/
	$("#to-top").hide();
	$(window).scroll(function(w){
		var w_scroll = $(this).scrollTop();
		if( w_scroll > 200 ){
			$("#to-top").fadeIn("slow");
		}else{
			$("#to-top").fadeOut("slow");
		}
	});
	$("#to-top").click(function(e){
		e.preventDefault();
		$('body,html').stop().animate({scrollTop:0},1000);
		return false;
	});
	
	/**/
});