<?php if ( comments_open() ) : ?>

<div id="respond">
	<h4 class="text-success">
		<?php comment_form_title( __('Thêm bình luận.','html5reset'), __('Trả lời %s','html5reset') ); ?>
    </h4>
	<div class="cancel-comment-reply">
		<?php cancel_comment_reply_link(); ?>
	</div>

	<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
		<p><?php _e('Bạn phải','html5reset'); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('đăng nhập','html5reset'); ?></a> 
		   <?php _e('để viết bình luận.','html5reset'); ?></p>
	<?php else : ?>
    <br />
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( is_user_logged_in() ) : ?>
			<p><?php _e('Đã đăng nhập bằng','html5reset'); ?> 
            	<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. 
                <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Đăng xuất tài khoản"><?php _e('Đăng xuất','html5reset'); ?></a></p>
		<?php else : ?>
			<div class="row">
            	<div class="col-lg-2">
                	<label for="author" class="label brown"><?php _e('Tên','html5reset'); ?> <?php if ($req) echo "<span class='red'> * </span>"; ?></label>
                </div>
                <div class="col-lg-10">
					<input class="form-control" type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" 
					<?php if ($req) echo "aria-required='true'"; ?> />
                </div>
			</div><br />
			<div class="row">
            	<div class="col-lg-2">
                	<label class="label brown" for="cm-email"><?php _e('Mail (không hiển thị)','html5reset'); ?> <?php if ($req) echo "<span class='red'> * </span>"; ?></label>
                </div>
                <div class="col-lg-10">
                	<input type="text" class="form-control" name="email" id="cm-email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
                </div>
			</div><br />
			<div class="row">
            	<div class="col-lg-2">
                	<label class="label brown" for="url"><?php _e('Website','html5reset'); ?></label>
                </div>
                <div class="col-lg-10">
                	<input class="form-control" type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
                </div>
			</div><br />
		<?php endif; ?>
		<div class="row">
        	<div class="col-lg-2"></div>
        	<div class="col-lg-10">
				<textarea class="form-control" name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea>
            </div>
		</div><br />
		<div class="row">
        	<div class="col-lg-2"></div>
            <div class="col-lg-3">
				<input class="form-control" name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Bình luận','html5reset'); ?>" />
				<?php comment_id_fields(); ?>
            </div>
		</div>
		<?php do_action('comment_form', $post->ID); ?>
	</form>
	<?php endif; // If registration required and not logged in ?>
</div>
<br />
<?php endif; ?>