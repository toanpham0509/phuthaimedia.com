<div id="footer">
	<div id="content">
    	<div class="item">
        	<h2 class="title"><?php echo get_cat_name( 7 ); ?></h2>
            <ul class="list">
            	<?php
					wp_reset_postdata(); 
                	$args = array(
						"parent" => 7
					);
					$my_cats = get_categories($args);
					foreach($my_cats as $my_cat){
						?>
						<li><h2><a href="<?php echo get_category_link($my_cat->term_id); ?>" title="<?php $my_cat->name ?>"><?php echo $my_cat->name ?></a></h2></li>
						<?php
					}
				?>
            </ul>
            <!-- <img src="<?php bloginfo( "template_url" ); ?>/images/logo.png" /> -->
            <img class="footer-icon" src="<?php bloginfo( "template_url" ); ?>/images/facebook.png" />
            <img class="footer-icon" src="<?php bloginfo( "template_url" ); ?>/images/g+.png" />
            <img class="footer-icon" src="<?php bloginfo( "template_url" ); ?>/images/twitter-icon.png" />
        </div>
        <div class="item">
        	<h2 class="title">Dịch vụ quảng cáo</h2>
            <ul class="list">
            	<?php
					wp_reset_postdata(); 
                	$args = array(
						"category" => 27,
						"tag"      => "footer"
					);
					$my_cats = get_posts($args);
					foreach($my_cats as $my_cat){
						?>
						<li><h2><a href="<?php echo get_permalink( $my_cat->ID ); ?>" title="<?php $my_cat->name ?>"><?php echo $my_cat->post_title ?></a></h2></li>
						<?php
					}
				?>
            </ul>
        </div>
        <div class="item">
        	<h2 class="title">Liên hệ</h2>
            <h1 style="margin:0;"><p style="font-size:16px;"><b>Công ty cổ phần truyền thông Phú Thái</b></p></h1>
	            <h2 style="font-size:13px;margin:0;">
    	        <img src="<?php bloginfo("template_url"); ?>/images/home-icon.png" class="footer-icon" /> P.805-N5C, KDT Trung Hòa - Nhân Chính, Thanh Xuân, Hòa Nội. <br />
        	    <span style="text-indent:30px;display:block;">VPGD: P.805-24T1, KDT Trung Hòa - Nhân Chính, Cầu Giấy, Hà Nội.</span><br />
            	<img src="<?php bloginfo("template_url"); ?>/images/icon-phone.png" class="footer-icon" /> (84-4) 2215 4586 <br />
	            <img src="<?php bloginfo("template_url"); ?>/images/icon-fax.gif" class="footer-icon" /> (84-4) 6281 69111 <br />
    	        <img src="<?php bloginfo("template_url"); ?>/images/icon-email.png" class="footer-icon" />  info@phuthaimedia.com.vn<br />
        	    </h2>
        </div>
    </div>
    <div class="clear"></div>
</div>
</body>
</html>
