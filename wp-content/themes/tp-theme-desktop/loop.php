<?php 
function loop(){
	if( have_posts() ) :
                while( have_posts() ):
                    the_post();
					echo "<div class='item-news active-1' style='margin-right:5px;margin-top:0px;margin-bottom:15px;width:346px;'>";
							?>
                            <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                            <?php 
								if( has_post_thumbnail() ){
									the_post_thumbnail();
                                }else { ?>
                                	<img src="<?php bloginfo("template_url"); ?>/images/no_image.png" />
                                <?php } ?>
                            </a>
							<?php
						echo "<div class='title'>";
							echo "<h2 style='font-size:18px;margin-top:10px;'>";
							echo "<a href='";
								the_permalink();
							echo "'>";
								the_title();
							echo "</a>";
							echo "</h2>";
						echo "</div>";
						echo "<div class='excerpt'>";
							tp_the_excerpt(100);
						echo "</div>";
						?>
                        <?php
					echo "</div>";
                endwhile;
            endif;
        wp_reset_query();
}
?>
<br />
<div id="content" style="margin-top:-10px;">
	<?php wp_reset_query(); ?>
	<?php
        //Title.
        if( is_category() ) :	
            //echo "<div class='icon-category'><div class='content'>";
           // echo "<a href=\"". get_category_link( get_the_title() ) ."\">";
           //     single_cat_title();
           // echo "</a>";
           // echo "</div></div>";
        elseif( is_tag() ) :
            echo "<div class='icon-tagss'><div class='content'>";
                echo __('Từ khóa <i class="glyphicon glyphicon-play"></i> ', 'shprinkone') . ': <b><a href="">' . single_tag_title('', false); 
                echo "</a></b>";
            echo "</div></div>";
        elseif( is_author() ) :
            echo "<div class='icon-author'><div class='content'>";
                echo "Đăng bởi <b><a href=''>"; 
                    printf(esc_attr__('%s ', 'shprinkone'), get_the_author()); 
                echo "</a></b>";
                echo "<small class=\"nickname\"><i>(";
                    the_author_meta('nickname'); 
                echo " )</i></small>";
            echo "</div></div>";
        elseif ( is_404() ):
            
        elseif ( is_search() ) :
            echo "<h3 class='text-success' style=\"margin-top:0px\">Kết quả tìm kiếm cho: <a href=\"\"><b>";
			printf( __(' %s', 'sprinkone'), get_search_query()); 
			echo "</b></a></h3>";
            get_search_form();
			echo "<br /><br />";
        endif;
    ?>
    <div class="box-new">
    	<div class="body" style="padding:0px 0px;margin-left:-20px;">
        <?php wp_reset_query(); ?>
		<?php 
            //Loop.
			if( is_category() ){
				
				$category = get_the_category();
				$category = $category[0];
				$cat_id = $category->term_ID;
				if( $category -> cat_ID == 16 && $cat_id == 16 ){
					get_template_part("category-san-pham");
				}else if( $category -> cat_ID == 14 ) {
					get_template_part("category-doi-tac");
				}else {
					loop();
				}
				
			}else {
				loop();
			} 
		?>
    	</div>
    </div>
</div>