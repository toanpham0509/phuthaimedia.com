<div id="content">
    <div class="">
    	<div class="box-new">
        	<div class="header">
            	<div class="title">
                	<h4 class="title">
                	<a href="<?php  echo get_category_link(7); ?>" title="<?php echo get_cat_name(7); ?>" style="margin-left:-10px">
						<?php echo get_cat_name(7); ?>
                    </a>
                    </h4>
                </div>
                <a href="<?php  echo get_category_link(7) ?>"><div class="background-right-box-news"></div></a>
            </div>
            <div class="body">
                <div id="slider-1" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    <?php
						tp_get_post_in_cat_for_slider(7, 9);
					?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-1" role="button" data-slide="prev">
                    	<img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-1" role="button" data-slide="next">
                    	<img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
            </div>
        </div>
        <div class="box-new">
        	<div class="header">
            	<div class="title">
                	<h4 class="title">
                	<a href="<?php echo get_category_link(15); ?>" title="<?php echo get_cat_name(15); ?>">
                    	<?php 
							if( get_bloginfo('language') == 'vi' )
								echo "TIN MỚI";
							else
								echo "NEWS";
							
						?>
                    </a>
                    </h4>
                </div>
                <a href="<?php echo get_category_link(15) ?>"><div class="background-right-box-news"></div></a>
            </div>
            <div class="body">
                <div id="slider-2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
							tp_get_post_in_cat_for_slider(15, 9);
						?>
                    </div>                  
                    <!-- Controls -->
                    <a class="left" href="#slider-2" role="button" data-slide="prev">
                      <img src="<?php bloginfo("template_url"); ?>/images/btn-preview.png" class="icon-prev slider-control" />
                    </a>
                    <a class="right" href="#slider-2" role="button" data-slide="next">
                      <img src="<?php bloginfo("template_url"); ?>/images/button-next.png" class="icon-next slider-control" />
                    </a>
                </div>
            </div>
        </div>
        <div class="box-new" style="margin-bottom:20px;">
        	<div class="header">
            	<div class="title">
                	<h4 class="title">
                	<a href="<?php echo get_category_link(14); ?>" title="<?php echo get_cat_name(14); ?>">
						<?php echo get_cat_name(14); ?>
                    </a>
                    </h4>
                </div>
                <a href="<?php echo get_category_link(14) ?>"><div class="background-right-box-news"></div></a>
            </div>
            <div class="body">
            	<div class="cuslogo list-icon-image ">
                  		<div class="cycle-slideshow" data-cycle-fx=carousel data-cycle-timeout=2000 data-cycle-carousel-visible=6 data-cycle-carousel-fluid=true data-cycle-slides="div">
                        <?php
                        	$args = array (
								"cat" => 14,
								"post-status" => "publish"
							);
							$the_query = new WP_Query($args);
							if( $the_query->have_posts() ){
								while( $the_query->have_posts() ){
									$the_query->the_post();
									if( has_post_thumbnail() ){
										echo "<div class=\"span2\">";
										?>
                                         <a href="<?php echo get_the_content() ?>" target="_blank" title="<?php the_title(); ?>">
                                         <?php the_post_thumbnail(); ?>
                                         </a>
                                         <?php
										echo "</div>";
										?>
                                        <?php
									}
								}
									
							}
						?>
               			</div>
              	  </div>
            </div>    
        </div>
    </div>
</div>