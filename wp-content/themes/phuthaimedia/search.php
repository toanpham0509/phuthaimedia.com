<?php get_header(); ?>
          <?php  
            $id_news = 4;
            $item_count = 0;
            $limit_post_onrow = 3; ?>
            <div class="col-md-12 col-xs-12">
            <h2>Kết quả tìm kiếm</h2>
            <?php if ( have_posts() ) { 
            ?>
            <?php while ( have_posts() ) { the_post(); ?>
              <?php if( $item_count % $limit_post_onrow ==0 ){ ?>
                <div class="row text-center">
              <?php }?>
                  <div class="col-md-4 col-xs-4">
                     <a href="<?php the_permalink(); ?>">
                          <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 280, 150) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                      </a>
                      <h3 style="color:black"><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h3>
                  </div>
              <?php if( $item_count % $limit_post_onrow ==2 ) { ?>
                </div> 
              <?php } ?>
              <?php $item_count++; ?>
            <?php } // End WHILE Loop
              if( ( $item_count - 1 ) % $limit_post_onrow !=2){ ?>
                </div>
            <?php }
            } else {
              printf( __( '<p>Không có kết quả nào !<a href="%s"> Quay lại trang chủ </a></p>', 'phuthaimedia' ), get_home_url() );
              }
            ?>
          </div>

<?php get_footer(); ?>