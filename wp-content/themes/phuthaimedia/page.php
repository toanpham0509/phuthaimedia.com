<?php get_header(); ?>
          <div class="col-md-12 col-xs-12">
            <?php if ( have_posts() ) { ?>
            <?php while ( have_posts() ) { the_post(); ?>
              <div class="page-header">
                <h2><?php the_title(); ?></h2>
                <ul class="list-inline text-muted">
                  <!-- <li><?php echo get_avatar( get_the_author_meta('ID'), 32 ); ?> <small><?php echo get_the_author(); ?></small></li>
                  <?php $cat = get_the_category(); $cat = $cat[0]; ?>
                  <li><small><span class="glyphicon glyphicon-list"></span> <a class="glink" href="<?php echo get_category_link( $cat->cat_ID );?>"><?php echo $cat->cat_name; ?></a></small></li>
                  <li><small><span class="glyphicon glyphicon-calendar"></span> <?php the_time( get_option( 'date_format' ) ); ?></small></li>
                  <li><small><span class="glyphicon glyphicon-eye-open"></span> <?php if(function_exists('the_views')) :  { the_views(); }  endif; ?></small></li>
                  <li><small><span class="glyphicon glyphicon-comment"></span> <a href="#comments"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></small></li>
                  <?php edit_post_link( __( 'Edit', 'web' ),'<li><small><span class="glyphicon glyphicon-pencil"></span> ','</small></li>' ); ?> -->
                </ul>
              </div>
              <div class="post_text" >
                <?php the_content('<p>'.__('Continue reading this post','web').'</p>'); ?>
                <?php wp_link_pages( array( 'before' => '<p><strong>' . __( 'Pages','web' ) . ':</strong> ', 'after' => '</p>', 'next_or_number' => 'number' ) ); ?>
              </div> <!--END post-text-->
              <small class="pull-right" style="margin-top:30px;"><span class="glyphicon glyphicon-calendar"></span> <?php the_time( get_option( 'date_format' ) ); ?></small>
            <?php } // End WHILE Loop
            } else {
              printf( __( '<p>Lost? Go back to the <a href="%s">home page</a></p>', 'goctamhon' ), get_home_url() );
              }
            ?>
          </div>
<?php get_footer(); ?>