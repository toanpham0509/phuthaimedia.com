<?php get_header(); ?>
        <div class="col-md-12 col-xs-12" style="margin: 0;padding: 0;" >
            <div class="row" style="margin: 0;padding: 0;" >
                <div class="col-md-12 col-xs-12" style="margin: 0;padding: 0;">
                    <div id="bannerSlider" class="carousel slide" data-ride="carousel" style="margin: 0;padding: 0;">
                <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#bannerSlider" data-slide-to="0" class="active"></li>
                        <li data-target="#bannerSlider" data-slide-to="1" class=""></li>
                        <li data-target="#bannerSlider" data-slide-to="2" class=""></li>
                      </ol>
                      <div class="carousel-inner" style="margin: 0;padding: 0;">
                      <?php  
                         $feat_id = 16;
                          $limit = 30;
                          $active = true;
                          $the_query = new WP_Query( 'cat=' . $feat_id . '&showposts=' . $limit );
                           while ( $the_query->have_posts()) : $the_query->the_post();
                                if ($active) { ?>
                                <div class="item active">
                                <?php $active = false; } else { ?>
                                <div class="item">
                                <?php } ?>
                                  <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                      <div class="carousel-caption" style="left:0px !important;right:0px !important;padding: 0px !important;top: 0px;" >
                                          <a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 960, 300) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                                          </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                          <?php endwhile; 
                      ?>
                    </div>
                    <a class="left carousel-control" style="z-index: 99 !important" href="#bannerSlider" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" ></span></a>
                    <a class="right carousel-control" style="z-index: 99 !important" href="#bannerSlider" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                  </div>
                </div>
            </div>
        <!--End banner slider-->
            <div class="row slidehome" style="margin: 0">
              <div class="col-md-12 col-xs-12 text-center" style="margin: 0;padding: 0;">
                  	<?php $feat_id = 7;
              		if( get_bloginfo('language') == 'vi'){
                       $url = home_url() . '/?cat=' . $feat_id;
                    } else {
                        $url = home_url() . '/?cat=' . $feat_id .'&lang=en';
                    } 
					  echo "<br />";
                      echo ' <a href="' . $url .'">';
                      echo '<h3 style="font-size:20px !important;"><b>'. get_cat_name( $feat_id ).' </b></h3>';
                      echo '</a>';
                    ?>
                  <div id="newsSlider" class="carousel slide myslider " data-ride="carousel">
                  <!-- Indicators -->
                  <div class="carousel-inner">
                        <?php
                          $limit = 30;
                          $active = true;
                          $item_count = 0;
                          $limt_post_onslide = 3;
                          $the_query = new WP_Query( 'cat=' . $feat_id . '&showposts=' . $limit );
                          while ( $the_query->have_posts()) : $the_query->the_post();
                            $do_not_duplicate = $post->ID; ?>
                          <?php if($item_count%$limt_post_onslide==0) { ?>
                            <?php if ($active) { ?>
                            <div class="item active">
                            <?php $active = false; } else { ?>
                            <div class="item">
                            <?php } ?>
                            <div class="row"> 
                            <div class="carousel-caption" > 
                          <?php } ?>
                              <div class="col-md-4 col-xs-4 ">
                              	<div  style="min-height: 300px;">
                              		<a href="<?php the_permalink(); ?>">
                                  		<img src="<?php echo gth_resize_img(gth_post_thumbnail(), 250, 150) ?>" height="150" width="250" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                                	</a>
                                	<h4 style="color:black"><a href="<?php the_permalink(); ?> " class="title"><?php the_title(); ?></a></h4>
                                	<?php the_excerpt(); ?>   
                              	</div>
                                <div class="pull-right">
                                	<?php  
                                		if( get_bloginfo('language') == 'vi'){ ?>
				                       		<a href="<?php the_permalink(); ?> " style="color:black">Xem tiếp >></a>
				                   		   <?php } else { ?>
				                        	<a href="<?php the_permalink(); ?> " style="color:black">More >></a>
				                          <?php  } ?>   
                                </div> 
                              </div>
                           <?php if($item_count%$limt_post_onslide == 2 ) { ?>
                           </div><!--end carousel-caption-->
                           </div><!--end row-->
                          </div><!--end item-->
                          <?php }?>
                          <?php  $item_count++; ?>
                  <?php endwhile; 
                   if( ( $item_count - 1 ) % $limt_post_onslide != 2) { ?>
                      </div><!--end carousel-caption-->
                     </div><!--end row-->
                    </div><!--end item-->
                   <?php } ?>
                  </div><!--end div carousel-inner-->
                  <a class="left carousel-control" href="#newsSlider" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                  <a class="right carousel-control" href="#newsSlider" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div> <!--End div slider-->
              </div>
            </div>
      <!--End news slider-->
            <div class="row slidehome"style="margin: 0">
              <div class="col-md-12 col-xs-12 text-center" style="margin: 0;padding: 0;">
              	<?php $feat_id = 15;
              		  $id_infor = 4;
              		if( get_bloginfo('language') == 'vi'){
                      $url = home_url() . '/?cat=' . $feat_id;
                  } else {
                      $url = home_url() . '/?cat=' . $feat_id .'&lang=en';
                  } 
                  echo '<br /> <a href="' . $url .'">';
                  echo '<h3 style="font-size:20px !important;"><b>'. get_cat_name( $feat_id ).' </b></h3>';
                  echo '</a>';
              	 ?>
                  <!--intro banner-->
                  <div id="introSlider" class="carousel slide myslider" data-ride="carousel" >
                    <!-- Indicators -->
                    <div class="carousel-inner">
                     <?php
                      $limit =30;
                      $active = true;
                      $item_count = 0;
                      $limt_post_onslide = 3;
                      $the_query = new WP_Query( 'cat=' . $feat_id . '&showposts=' . $limit );
                      while ($the_query->have_posts()) : $the_query->the_post();
                        $do_not_duplicate = $post->ID; ?>
                      <?php if($item_count%$limt_post_onslide==0) { ?>
                        <?php if ($active) { ?>
                        <div class="item active">
                        <?php $active = false; } else { ?>
                        <div class="item">
                        <?php } ?>
                        <div class="row"> 
                        <div class="carousel-caption" > 
                      <?php } ?>
                          <div class="col-md-4 col-xs-4">
                          	<div style="min-height: 150px;">
                          		<h4><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h4><br />
                              	<?php the_excerpt(); ?>
                              
                          	</div>
                          	<div class="pull-right">
                              <?php  
                                    if( get_bloginfo('language') == 'vi'){ ?>
                                  <a href="<?php the_permalink(); ?> " style="color:black">Xem tiếp >></a>
                                 <?php } else { ?>
                                  <a href="<?php the_permalink(); ?> " style="color:black">More >></a>
                              <?php  } ?>   
                            </div>  
                          </div>

                       <?php if( $item_count % $limt_post_onslide == 2 ) { ?>
                       </div><!--end carousel-caption-->
                       </div><!--end row-->
                      </div><!--end item-->
                      <?php }?>
                      <?php  $item_count++; ?>
                    <?php endwhile; 
                      if( ( $item_count - 1 ) % $limt_post_onslide != 2) { ?>
                        </div><!--end carousel-caption-->
                       </div><!--end row-->
                      </div><!--end item-->
                     <?php } ?>
                  </div><!--carousel-inner-->
                  <a class="left carousel-control" href="#introSlider" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                  <a class="right carousel-control" href="#introSlider" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div><!-- introSlider -->
              </div>
            </div>
            <div class="cuslogo container">

            <?php $post_id_cus = 14;
             if( get_bloginfo('language') == 'vi'){
                      $url = home_url() . '/?cat=' . $post_id_cus;
                  } else {
                      $url = home_url() . '/?cat=' . $post_id_cus .'&lang=en';
              } 
              echo ' <a href="' . $url .' ">';
              echo '<h3  style="font-size:20px !important;"><b>'. get_cat_name( $post_id_cus ).' </b></h3>';
              echo '</a>';
            ?>
              <div class="row cycle-slideshow" data-cycle-fx=carousel data-cycle-timeout=1000 data-cycle-carousel-visible=4 data-cycle-carousel-fluid=true data-cycle-slides="div">
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/inet.jpg"></div>
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/la34.jpg"></div>
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/qtv_new.jpg"></div>
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/sctv.jpg"></div>
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/thp.jpg"></div>
                <div class="span2"><img src="<?php echo get_template_directory_uri(); ?>/images/cus/vtv_new.jpg"></div>
              </div>
            </div>
            <script src="http://malsup.github.com/jquery.cycle2.js"></script>
            <script src="http://malsup.github.com/jquery.cycle2.carousel.js"></script>
          </div>
          
<?php get_footer(); ?>