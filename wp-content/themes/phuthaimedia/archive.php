<?php get_header(); ?>
        <?php  
            $is_news = false;
            $id_news = 4;
            $id_product = 7;
            $item_count = 0;
            $limit_post_onrow = 3;
            $category = get_category( get_query_var( 'cat' ) );
            if( $category->count >1 ){
            $cat_id = $category->cat_ID;
            if($cat_id == $id_news ){
                 $is_news = true; 
            }?>
             <div class="col-md-12 col-xs-12" style="background-color: white !important">
                <?php 
                    if( !$is_news ){ ?>
                      <h2><?php single_cat_title( '', true ); ?></h2>
                <?php } else {
                      echo ' <a href="<?php  ?>">';
                      echo '<h2>'. get_cat_name( $cat_id ).' </h2>';
                      echo '</a>';
                    } 
                  if ( have_posts() ) { while ( have_posts() ) { the_post(); 
                  if( $item_count % $limit_post_onrow ==0 ){ ?>
                    <div class="row text-center">
                  <?php }?>
                      <div class="col-md-4 col-xs-4">
                         <a href="<?php the_permalink(); ?>">
                              <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 250, 150) ?>" height="150" width="250" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                          </a>
                          <h3 style="color:black"><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h3>
                      </div>
                  <?php if( $item_count % $limit_post_onrow ==2 ) { ?>
                    </div>
                    <?php 
                      if( !$is_news){ ?>
                      
                    <?php } else if( $item_count < $limit_post_onrow && $is_news){ ?>

                    <?php }
                  }?>
                  <?php $item_count++; ?>
                <?php } // End WHILE Loop
                  if( ( $item_count - 1 ) % $limit_post_onrow !=2){ ?>
                    </div>
                <?php }
                } else {
                  printf( __( '<p>Lost? Go back to the <a href="%s">home page</a></p>', 'phuthaimedia' ), get_home_url() );
                  }
                ?>
              </div>
            <?php  get_footer(); 
                   return ; 
                   } elseif( $category->cat_ID != 7 ) { ?>
                  <div class="col-md-12 col-xs-12" style="background-color: white !important">

                  <?php if ( have_posts() ) { ?>
                  <?php while ( have_posts() ) { the_post(); ?>
                    <div class="page-header">
                      <h2><?php the_title(); ?></h2>
                      <ul class="list-inline text-muted">
                        <!-- <li><?php echo get_avatar( get_the_author_meta('ID'), 32 ); ?> <small><?php echo get_the_author(); ?></small></li>
                        <?php $cat = get_the_category(); $cat = $cat[0]; ?>
                        <li><small><span class="glyphicon glyphicon-list"></span> <a class="glink" href="<?php echo get_category_link( $cat->cat_ID );?>"><?php echo $cat->cat_name; ?></a></small></li>
                        <li><small><span class="glyphicon glyphicon-calendar"></span> <?php the_time( get_option( 'date_format' ) ); ?></small></li>
                        <li><small><span class="glyphicon glyphicon-eye-open"></span> <?php if(function_exists('the_views')) :  { the_views(); }  endif; ?></small></li>
                        <li><small><span class="glyphicon glyphicon-comment"></span> <a href="#comments"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></small></li>
                        <?php edit_post_link( __( 'Edit', 'web' ),'<li><small><span class="glyphicon glyphicon-pencil"></span> ','</small></li>' ); ?> -->
                      </ul>
                    </div>
                    <div class="post_text" >
                      <?php the_content('<p>'.__('Continue reading this post','web').'</p>'); ?>
                      <?php wp_link_pages( array( 'before' => '<p><strong>' . __( 'Pages','web' ) . ':</strong> ', 'after' => '</p>', 'next_or_number' => 'number' ) ); ?>
                    </div> <!--END post-text-->
                    <small class="pull-right" style="margin-top:30px;"><span class="glyphicon glyphicon-calendar"></span> <?php the_time( get_option( 'date_format' ) ); ?></small>
                  <?php } // End WHILE Loop
                  } else {
                    printf( __( '<p>Lost? Go back to the <a href="%s">home page</a></p>', 'goctamhon' ), get_home_url() );
                    }
                  ?>
                </div>
            <?php   get_footer();
                    return ; } ?>
            <?php  
              hierarchical_category_tree( $id_product ); // the function call; 0 for all categories; or cat ID  
              function hierarchical_category_tree( $cat ) {
                $next = get_categories('hide_empty=false&orderby=name&order=ASC&parent=' . $cat);
                if( $next ) :    
                  foreach( $next as $cat ) : ?>
                                <div class="row slidehome" style="margin: 0">
                                  <div class="col-md-12 col-xs-12 text-center" style="margin: 0;padding: 0;">
                                      <h2><?php echo $cat->name ?></h2>
                                      <div id="slide<?php print $cat->term_id; ?>" class="carousel slide myslider " data-ride="carousel">
                                      <!-- Indicators -->
                                      <div class="carousel-inner">
                                            <?php
                                              $feat_id = $cat->term_id ; 
                                              $limit = 30;
                                              $active = true;
                                              $item_count = 0;
                                              $limt_post_onslide = 3;
                                              $the_query = new WP_Query( 'cat=' . $feat_id . '&showposts=' . $limit );
                                              while ( $the_query->have_posts()) : $the_query->the_post();
                                                $do_not_duplicate = $post->ID; ?>
                                              <?php if($item_count%$limt_post_onslide==0) { ?>
                                                <?php if ($active) { ?>
                                                <div class="item active">
                                                <?php $active = false; } else { ?>
                                                <div class="item">
                                                <?php } ?>
                                                <div class="row"> 
                                                <div class="carousel-caption" > 
                                              <?php } ?>
                                                  <div class="col-md-4 col-xs-4 ">
                                                    <div  style="min-height: 300px;">
                                                      <a href="<?php the_permalink(); ?>">
                                                          <img src="<?php echo gth_resize_img(gth_post_thumbnail(), 250, 150) ?>" height="150" width="250" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                                                      </a>
                                                      <h3 style="color:black"><a href="<?php the_permalink(); ?> " class="title"><?php the_title(); ?></a></h3>
                                                      <?php the_excerpt(); ?>   
                                                    </div>
                                                    <div class="pull-right">
                                                      <?php  
                                                          if( get_bloginfo('language') == 'vi'){ ?>
                                                            <a href="<?php the_permalink(); ?> " style="color:black">Xem tiếp >></a>
                                                         <?php } else { ?>
                                                            <a href="<?php the_permalink(); ?> " style="color:black">More >></a>
                                                    <?php } ?>   
                                                    </div> 
                                                  </div>
                                               <?php if($item_count%$limt_post_onslide == 2 ) { ?>
                                               </div><!--end carousel-caption-->
                                               </div><!--end row-->
                                              </div><!--end item-->
                                              <?php }?>
                                              <?php  $item_count++; ?>
                                      <?php endwhile; 
                                       if( ( $item_count - 1 ) % $limt_post_onslide != 2) { ?>
                                          </div><!--end carousel-caption-->
                                         </div><!--end row-->
                                        </div><!--end item-->
                                       <?php } ?>
                                      </div><!--end div carousel-inner-->
                                      <a class="left carousel-control" href="#slide<?php print $cat->term_id; ?>" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                      <a class="right carousel-control" href="#slide<?php print $cat->term_id; ?>" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div> <!--End div slider-->
                                  </div>
                                </div>
                    <?php 
                    hierarchical_category_tree( $cat->term_id );
                  endforeach;    
                endif;
                    echo '</li></ul>'; echo "\n";
              } 
            ?>
<?php get_footer(); ?>