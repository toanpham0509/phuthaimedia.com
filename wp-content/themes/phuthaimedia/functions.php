<?php

/******************* THEME SETUP ***********************/

update_option('siteurl','http://localhost/koolsoft3.8/');
update_option('home','http://localhost/koolsoft3.8/');

add_theme_support("post-thumbnails");
add_theme_support( 'automatic-feed-links' ); // auto add RSS feed links to head


// register menus
function register_menus() {
    register_nav_menus(
        array(
            'main-menu'     => __('Main Menu')
        )
    );
}
add_action('init', 'register_menus');

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<a class="more-link" href="' . get_permalink() . '">Xem tiếp >></a>';
}

////////////////////////////////////////////////////////

/**
 * Add filter for Jetpack to get image from post contain video or embed content
 */
function jetpeckme_get_image($media, $post_id) {
    if ($media) {
        return $media;
    }
    $embed = esc_html( get_post_meta( $post_id, 'embed', true ) );
    $url = esc_url( woo_get_video_image( $embed ) );
    if (empty($url)) {
        $url = get_bloginfo('template_url') . "/images/default.jpg";
    }

    $images[] = array(
                    'type'  => 'image',
                    'from'  => 'html',
                    'src'   => $url,
                    'href'  => '', // No link to apply to these. Might potentially parse for that as well, but not for now
                );
    return $images;
}
add_filter('jetpack_images_get_images', 'jetpeckme_get_image', 11, 2);


////////////////////////////////////////////////////////

/************* USEFUL FUNCTION *************************/

// Register Custom Navigation Walker
require_once('functions/wp_bootstrap_navwalker.php');

/*
 * Display Image from the_post_thumbnail or the first image of a post else 
 * display a default Image
 * USAGE: <?php echo gth_post_thumbnail(); ?>
 */
function gth_post_thumbnail() {
    if (has_post_thumbnail()) {
        $image_id = get_post_thumbnail_id();
        $image_url = wp_get_attachment_image_src($image_id, $size);
        $image_url = $image_url[0];
    } else {
        global $post, $posts;
        $image_url = '';
        ob_start();
        ob_end_clean();
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        $image_url = $matches [1] [0];

        if (empty($image_url)) {
            $embed = esc_html( get_post_meta( $post->ID, 'embed', true ) );
            $image_url = esc_url( woo_get_video_image( $embed ) );
        }
        
        //Defines a default image
        if(empty($image_url)) {
            $image_url = get_bloginfo('template_url') . "/images/default.jpg";
        }
    }
    return $image_url;
}

/* Get thumbnail from Video Embed code */
if ( ! function_exists( 'woo_get_video_image' ) ) {
function woo_get_video_image( $embed ) {
    $video_thumb = '';

    // YouTube - get the video code if this is an embed code (old embed)
    preg_match( '/youtube\.com\/v\/([\w\-]+)/', $embed, $match );

    // YouTube - if old embed returned an empty ID, try capuring the ID from the new iframe embed
    if( ! isset( $match[1] ) )
        preg_match( '/youtube\.com\/embed\/([\w\-]+)/', $embed, $match );

    // YouTube - if it is not an embed code, get the video code from the youtube URL
    if( ! isset( $match[1] ) )
        preg_match( '/v\=(.+)&/', $embed, $match );

    // YouTube - get the corresponding thumbnail images
    if( isset( $match[1] ) )
        $video_thumb = "http://img.youtube.com/vi/" . urlencode( $match[1] ) . "/0.jpg";

    // return whichever thumbnail image you would like to retrieve
    return $video_thumb;
} // End woo_get_video_image()
}

/*
 * Resize image using timthumb
 */
function gth_resize_img($src, $width, $height, $zc='1', $quality='90') {
    $img_url = esc_url( get_template_directory_uri() . '/functions/thumb.php?src=' . $src . '&amp;w=' . $width . '&amp;h=' . $height . '&amp;zc=' . $zc . '&amp;q=' . $quality);
    return $img_url;
}

/*
 * Get avatar url
 */
function gth_avatar_url($get_avatar){
    preg_match("/src=[\"'](.*?)[\"']/i", $get_avatar, $matches);
    return $matches[1];
}

function gth_header_image() {
    $img_id = rand(1, 2);
    $img_url = esc_url(get_template_directory_uri(). '/images/headers/' . $img_id . '.jpg');
    return $img_url;
}

/*
 * Get related post
 */
/*-----------------------------------------------------------------------------------*/
/* woo_get_posts_by_taxonomy()
/*
/* Selects posts based on specified taxonomies.
/*
/* @since 4.5.0
/* @param array $args
/* @return array $posts
/*-----------------------------------------------------------------------------------*/
 
 function woo_get_posts_by_taxonomy ( $args = null ) {
    global $wp_query;
    
    $posts = array();
    
    /* Parse arguments, and declare individual variables for each. */
    
    $defaults = array(
                        'limit' => 5, 
                        'post_type' => 'any', 
                        'taxonomies' => 'post_tag, category', 
                        'specific_terms' => '', 
                        'relationship' => 'OR', 
                        'order' => 'DESC', 
                        'orderby' => 'date', 
                        'operator' => 'IN', 
                        'exclude' => ''
                    );
                    
    $args = wp_parse_args( $args, $defaults );
    
    extract( $args, EXTR_SKIP );
    
    // Make sure the order value is safe.
    if ( ! in_array( $order, array( 'ASC', 'DESC' ) ) ) { $order = $defaults['order']; }
    
    // Make sure the orderby value is safe.
    if ( ! in_array( $orderby, array( 'none', 'id', 'author', 'title', 'date', 'modified', 'parent', 'rand', 'comment_count', 'menu_order' ) ) ) { $orderby = $defaults['orderby']; }
    
    // Make sure the operator value is safe.
    if ( ! in_array( $operator, array( 'IN', 'NOT IN', 'AND' ) ) ) { $orderby = $defaults['operator']; }
    
    // Convert our post types to an array.
    if ( ! is_array( $post_type ) ) { $post_type = explode( ',', $post_type ); }
    
    // Convert our taxonomies to an array.
    if ( ! is_array( $taxonomies ) ) { $taxonomies = explode( ',', $taxonomies ); }
    
    // Convert exclude to an array.
    if ( ! is_array( $exclude ) && ( $exclude != '' ) ) { $exclude = explode( ',', $exclude ); }
    
    if ( ! count( (array)$taxonomies ) ) { return; }
    
    // Clean up our taxonomies for use in the query.
    if ( count( $taxonomies ) ) {
        foreach ( $taxonomies as $k => $v ) {
            $taxonomies[$k] = trim( $v );
        }
    }
    
    // Determine which terms we're going to relate to this entry.
    $related_terms = array();
    
    foreach ( $taxonomies as $t ) {
        $terms = get_terms( $t, 'orderby=id&hide_empty=1' );
        
        if ( ! empty( $terms ) ) {
            foreach ( $terms as $k => $v ) {
                $related_terms[$t][$v->term_id] = $v->slug;
            }
        }
    }
    
    // If specific terms are available, use those.
    if ( ! is_array( $specific_terms ) ) { $specific_terms = explode( ',', $specific_terms ); }
    
    if ( count( $specific_terms ) ) {
        foreach ( $specific_terms as $k => $v ) {
            $specific_terms[$k] = trim( $v );
        }
    }
    
    // Look for posts with the same terms.
    
    // Setup query arguments.
    $query_args = array();
    
    if ( $post_type ) { $query_args['post_type'] = $post_type; }
    
    if ( $limit ) {
        $query_args['posts_per_page'] = $limit;
        // $query_args['nopaging'] = true;
    }
    
    // Setup specific posts to exclude.
    if ( count( $exclude ) > 0 ) {
        $query_args['post__not_in'] = $exclude;
    }
    
    $query_args['order'] = $order;
    $query_args['orderby'] = $orderby;
    
    $query_args['tax_query'] = array();
    
    // Setup for multiple taxonomies.
    
    if ( count( $related_terms ) > 1 ) {
        $query_args['tax_query']['relation'] = $args['relationship'];
    }
    
    // Add the taxonomies to the query arguments.
    
    foreach ( (array)$related_terms as $k => $v ) {
        $terms_for_search = array_values( $v );
    
        if ( count( $specific_terms ) ) {
            $specific_terms_by_tax = array();
            
            foreach ( $specific_terms as $i => $j ) {
                if ( in_array( $j, array_values( $v ) ) ) {
                    $specific_terms_by_tax[] = $j;
                }
            }
            
            if ( count( $specific_terms_by_tax ) ) {
                $terms_for_search = $specific_terms_by_tax;
            }
        }
    
        $query_args['tax_query'][] = array(
            'taxonomy' => $k,
            'field' => 'slug',
            'terms' => $terms_for_search, 
            'operator' => $operator
        );
    }
    
    if ( empty( $query_args['tax_query'] ) ) { return; }
    
    $query_saved = $wp_query;
    
    $query = new WP_Query( $query_args );
    
    if ( $query->have_posts() ) {
        while( $query->have_posts() ) {
            $query->the_post();
            
            $posts[] = $query->post;
        }
    }
    
    $query = $query_saved;
    
    wp_reset_query();
 
    return $posts;
 } // End woo_get_posts_by_taxonomy()

/*-----------------------------------------------------------------------------------*/
/* Related Posts - related_posts
/*-----------------------------------------------------------------------------------*/
/*
This function requires at least WordPress Version 3.1.
Optional arguments:
 - limit: number of posts to show (default: 5)
 - image: thumbnail size, 0 = off (default: 0)
*/

function gth_related_posts ( $limit ) {
    global $post;
    
    wp_reset_query(); // Make sure we have a fresh query before we start.

    $post_type = get_post_type( $post->ID );
    
    $post_type_obj = get_post_type_object( $post_type );
    
    $taxonomies_string = 'post_tag, category';
    $taxonomies = array( 'post_tag', 'category' );
    
    if ( isset( $post_type_obj->taxonomies ) && count( $post_type_obj->taxonomies ) > 0 ) {
        $taxonomies_string = join( ', ', $post_type_obj->taxonomies );
        $taxonomies = $post_type_obj->taxonomies;
    }
    
    // Clean up our taxonomies for use in the query.
    if ( count( $taxonomies ) ) {
        foreach ( $taxonomies as $k => $v ) {
            $taxonomies[$k] = trim( $v );
        }
    }
    
    // Determine which terms we're going to relate to this entry.
    $related_terms = array();
    
    foreach ( $taxonomies as $t ) {
        $terms = get_the_terms( $post->ID, $t );
        
        if ( ! empty( $terms ) ) {
            foreach ( $terms as $k => $v ) {
                $related_terms[$t][$v->term_id] = $v->slug;
            }
        }
    }
    
    $specific_terms = array();
    foreach ( $related_terms as $k => $v ) {
        foreach ( $v as $i => $j ) {
            $specific_terms[] = $j;
        }
    }
    
    $query_args = array(
                    'limit' => $limit, 
                    'post_type' => $post_type, 
                    'taxonomies' => $taxonomies_string, 
                    'specific_terms' => $specific_terms, 
                    'order' => 'DESC', 
                    'orderby' => 'date', 
                    'exclude' => array( $post->ID )
                );
    
    $posts = woo_get_posts_by_taxonomy( $query_args );
    return $posts;
    
} // End gth_related_posts()

/*
Get Video
This function gets the embed code from the custom field
Parameters:
        $key = Custom field key eg. "embed"
        $width = Set width manually without using $type
        $height = Set height manually without using $type
        $class = Custom class to apply to wrapping div
        $id = ID from post to pull custom field from
*/

if ( ! function_exists( 'woo_embed' ) ) {
function woo_embed($args) {
    //Defaults
    $key = 'embed';
    $width = null;
    $height = null;
    $class = 'video';
    $id = null;

    if ( ! is_array( $args ) )
        parse_str( $args, $args );

    extract( $args );

    if( empty( $id ) ) {
        global $post;
        $id = $post->ID;
    }

    // Cast $width and $height to integer
    $width = intval( $width );
    $height = intval( $height );
        
    $custom_field = esc_textarea( get_post_meta( $id, $key, true ) );

    if ($custom_field) :
        $custom_field = html_entity_decode( $custom_field ); // Decode HTML entities.

        $org_width = $width;
        $org_height = $height;
        $calculated_height = '';
        $embed_width = '';
        $embed_height = '';

        // Get custom width and height
        $custom_width = esc_html( get_post_meta( $id, 'width', true ) );
        $custom_height = esc_html( get_post_meta( $id, 'height', true ) );

        //Dynamic Height Calculation
        if ($org_height == '' && $org_width != '') {
            $raw_values = explode(  ' ', $custom_field);

            foreach ( $raw_values as $raw ) {
                $embed_params = explode( '=', $raw );
                if ( $embed_params[0] == 'width' ) {
                    $embed_width = preg_replace( '/[^0-9]/', '', $embed_params[1]);
                }
                elseif ( $embed_params[0] == 'height' ) {
                    $embed_height = preg_replace( '/[^0-9]/', '', $embed_params[1]);
                }
            }

            $float_width = floatval( $embed_width );
            $float_height = floatval( $embed_height );
            @$float_ratio = $float_height / $float_width;
            $calculated_height = intval( $float_ratio * $width );
        }

        // Set values: width="XXX", height="XXX"
        if ( ! $custom_width ) $width = 'width="' . esc_attr( $width ) . '"'; else $width = 'width="' . esc_attr( $custom_width ) . '"';
        if ( $height == '' ) { $height = 'height="' . esc_attr( $calculated_height ) . '"'; } else { if ( ! $custom_height ) { $height = 'height="' . esc_attr( $height ) . '"'; } else { $height = 'height="' . esc_attr( $custom_height ) . '"'; } }
        $custom_field = stripslashes($custom_field);
        $custom_field = preg_replace( '/width="([0-9]*)"/' , $width , $custom_field );
        $custom_field = preg_replace( '/height="([0-9]*)"/' , $height, $custom_field );

        // Set values: width:XXXpx, height:XXXpx
        if ( ! $custom_width ) $width = 'width:' . esc_attr( $org_width ) . 'px'; else $width = 'width:' . esc_attr( $custom_width ) . 'px';
        if (  $height == '' ) { $height = 'height:' . esc_attr( $calculated_height ) . 'px'; } else { if ( ! $custom_height ) { $height = 'height:' . esc_attr( $org_height ) . 'px'; } else { $height = 'height:' . esc_attr( $custom_height ) . 'px'; } }
        $custom_field = stripslashes($custom_field);
        $custom_field = preg_replace( '/width:([0-9]*)px/' , $width , $custom_field );
        $custom_field = preg_replace( '/height:([0-9]*)px/' , $height, $custom_field );

        // Suckerfish menu hack
        $custom_field = str_replace( '<embed ', '<param name="wmode" value="transparent"></param><embed wmode="transparent" ', $custom_field );
        $custom_field = str_replace( '<iframe ', '<iframe wmode="transparent" ', $custom_field );
        $custom_field = str_replace( '" frameborder="', '?wmode=transparent" frameborder="', $custom_field );

        // Find and sanitize video URL. Add "wmode=transparent" to URL.
        $video_url = preg_match( '/src=["\']?([^"\' ]*)["\' ]/is', $custom_field, $matches );
        if ( isset( $matches[1] ) ) {
            $custom_field = str_replace( $matches[0], 'src="' . esc_url( add_query_arg( 'wmode', 'transparent', $matches[1] ) ) . '"', $custom_field );
        }

        $output = '';
        $output .= '<div class="'. esc_attr( $class ) .'">' . $custom_field . '</div>';

        return apply_filters( 'woo_embed', $output );
    else :
        return false;
    endif;
} // end function
} // end if

/*-----------------------------------------------------------------------------------*/
/* Add default filters to woo_embed() */
/*-----------------------------------------------------------------------------------*/

add_filter( 'woo_embed', 'do_shortcode' );

/*-----------------------------------------------------------------------------------*/
/* Depreciated - gth_get_embed - Get Video embed code from custom field */
/*-----------------------------------------------------------------------------------*/
// Depreciated
function gth_get_embed($key = 'embed', $width, $height, $class = 'embed-responsive embed-responsive-16by9', $id = null) {
    // Run new function
    return woo_embed( 'key='.$key.'&width='.$width.'&height='.$height.'&class='.$class.'&id='.$id );

}

// THIS GIVES US SOME OPTIONS FOR STYLING THE ADMIN AREA
function custom_css() {
   echo '<style type="text/css">
           .wp-editor-container textarea.wp-editor-area {
                font-family: "lucida grande", "Segoe UI", "Dejavu Sans", Sans-serif;
                font-size: 15px;
                line-height: 27px;
           }
         </style>';
}

add_action('admin_head', 'custom_css');

////////////////////////////////////////////////////////

?>