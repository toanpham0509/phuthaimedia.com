<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">
  <!-- Bootstrap -->
  <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.carousel.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/menu-main.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

    <script>
    var myCenter=new google.maps.LatLng(21.00828,105.802027);

    function initialize()
    {
    var mapProp = {
      center:myCenter,
      zoom:18,
      mapTypeId:google.maps.MapTypeId.ROADMAP
      };

    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker=new google.maps.Marker({
      position:myCenter,
      animation:google.maps.Animation.BOUNCE
      });

    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow({
      content:"PhuThai media!"
      });

    infowindow.open(map,marker);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
  <?php wp_head(); ?>
</head>
<body>
   <div class="container main" >
      <div class="row banner top-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bgtop.png');background-color:white;">
        <div class="col-md-8 ">
          <a class="brand" href="<?php echo home_url(); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
        </div>
        <div class="col-md-4" style="padding-right: 0px !important">
    
        </div>
         <div class="row">
        <div class="col-md-12 ">
             <?php
              if(is_404()) $url = get_option('home'); else $url = '';

              foreach(qtrans_getSortedLanguages() as $language)
              {
                $link = qtrans_convertURL('', $language);

                if($_SERVER["HTTPS"] == "on")
                  $link = preg_replace('#^http://#','https://', $link);
                  echo '<a href="'.$link.'">';
                  if(!strcmp($language ,"vi" )) {
                     echo '<img src="'.get_template_directory_uri().'/images/VN.png" height="22" width="22" title="'.$language.'" class="pull-right" style="margin-right: 30px;>';  
                  }else {
                     echo '<img src="'.get_template_directory_uri().'/images/EN.png" height="20" width="20"  title="'.$language.'" class="pull-right" style="margin-right: 10px;>';
                  }
                  echo '</a>';
                  echo '<a href="'.$link.'"><a/>'; 
              }
            ?>
      
        </div>
      </div>
      </div>
    <!-- Static navbar -->
    <div class="row menu-bar-top">
      <div class="col-md-9" style="font-size: 16px;">
        <nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
          <div class="container-fluid" style="font-size:16px">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <?php if( get_bloginfo('language') == 'vi'){
                      echo '<a class="navbar-brand" href="' . home_url() . '">TRANG CHỦ</a>';
                  } else {
                      echo '<a class="navbar-brand" href=" '. home_url() . '/?lang=en">Home</a>';
                  } ?>
                  
                 
              </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
              <?php
                wp_nav_menu( array(
                  'menu'              => 'main-menu',
                  'theme_location'    => 'main-menu',
                  'depth'             => 2,
                  'container'         => false,
                  'container_class'   => '',
                  'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'nav navbar-nav',
                  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                  'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </div>
      </nav>
    </div>
        <div class="col-md-3 " style="margin-top: 8px;padding-right:40px">
          <div class="nav navbar-nav navbar-right">
              <form role="search" method="get" id="searchform" class="searchform" action="<?php esc_url( home_url( '/' ) ); ?>">
                <div>
                 <!--  <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label> -->
                  <input type="text" class="form-control form-control-search" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s" id="s" />
                </div>
              </form>
          </div>
        </div>
    </div>



    <!-- Main component for a primary marketing message or call to action -->
    <div class="row homepage">