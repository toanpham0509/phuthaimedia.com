<?php
/*
*	Plugin Name: BK-disable-update
*	Plugin URI: http://bk-webs.com
*	Version: 1.0
*	Description: Plugin có nhiệm vụ vô hiệu quá tính năng cập nhật phiên bản mới cho các plugin.
*	Author: toanpham
*	Author URI: http://toanpham.tk
*/
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
?>
