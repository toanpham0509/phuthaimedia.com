<?php
/* Short code [form_book_place] <=> form book a place  */
if(!class_exists('BK_contact')) {
	class BK_contact {
		function __construct() {
			if(!function_exists('add_shortcode')) {
				return;
			}
			add_shortcode( 'bk-contact' , array(&$this, 'form_bk_contact_func') );
		}

		function form_bk_contact_func($atts = array(), $content = null) {
			extract(shortcode_atts(array('name' => 'World'), $atts));
			?>
<?php 
	/**
	*
	*	ReCaptcha
	*
	*
	*/
	include_once("recaptcha/recaptchalib.php");	
	$publickey = "6LeoyAATAAAAAMgEYEVOO3X3Mhd20F9B7JWEw3XL";
	$privatekey = "6LeoyAATAAAAAAwbvb1Rb5433ZDmTJI4ssZN17N8";
	$resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
?>
            <?php
				$errors = array();
				$message = "";				
            	if( isset($_POST['bk_send']) ) {
					
					/* name */
					if( isset($_POST['bk_name']) && strlen($_POST['bk_name']) > 0 && strlen($_POST['bk_name']) < 50 ) {
						$bk_name = strip_tags($_POST['bk_name']);
						if( strlen($bk_name) == 0 ) {
							$errors[] = "bk_name";
							$bk_name = null;
						}
					} else {
						$errors[] = "bk_name";
					}
					
					/* address */
					if( isset($_POST['bk_address']) && strlen($_POST['bk_address']) > 0 && strlen($_POST['bk_address']) < 200 ) {
						$bk_address = strip_tags($_POST['bk_address']);
						if( strlen($bk_address) == 0 ) {
							$errors[] = "bk_address";
							$bk_address = null;
						}
					} else {
						$errors[] = "bk_address";
					}
					
					/* phone */
					if( isset($_POST['bk_phone']) &&  (strlen($_POST['bk_phone']) == 10 || strlen($_POST['bk_phone']) == 11 ) ) {
						$length = strlen($_POST['bk_phone']);
						for($i = 0; $i < $length; $i ++ ) {
							if( !( $_POST['bk_phone'][$i] >= 0 && $_POST['bk_phone'][$i] <= 9 ) ) {
								$errors[] = "bk_phone";
							}
						}
						$bk_phone = strip_tags($_POST['bk_phone']);
						if( strlen($bk_phone) == 0 ) {
							$errors[] = "bk_phone";
							$bk_phone = null;
						}
					} else {
						$errors[] = "bk_phone";
					}

					//Email					
					if( isset($_POST['bk_email']) && filter_var($_POST['bk_email'], FILTER_VALIDATE_EMAIL) && strlen($_POST['bk_email']) < 50 ) {
						$bk_email = strip_tags($_POST['bk_email']);
						if( strlen($bk_email) == 0 ) {
							$errors[] = "bk_email";
							$bk_email = null;
						}
					} else {
						$errors[] = "bk_email";
					}
					
					//Title
					if( isset($_POST['bk_title']) && strlen($_POST['bk_title']) > 0 &&  strlen($_POST['bk_title']) < 200) {
						$bk_title = strip_tags($_POST['bk_title']);
						if( strlen($bk_title) == 0 ) {
							$errors[] = "bk_title";
							$bk_title = null;
						}
					} else {
						$errors[] = "bk_title";
					}
					
					//Content
					if( isset($_POST['bk_content']) && strlen($_POST['bk_content']) > 0 && strlen($_POST['bk_content']) < 2000 ) {
						$bk_content = htmlspecialchars(strip_tags($_POST['bk_content']), ENT_COMPAT, "utf-8");
						if( strlen($bk_content) == 0 ) {
							$errors[] = "bk_content";
							$bk_content = null;
						}
					} else {
						$errors[] = "bk_content";
					}
					
					if( empty($errors) ) {
						global $wpdb;
						if (!$resp->is_valid) {
    						// What happens when the CAPTCHA was entered incorrectly
    						$message = "<div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>Mã bảo mật không đúng. Vui lòng nhập đúng mã để gửi thư tới chúng tôi!</div><span class='text-success'></span>";
  						} else {
						//Oke. Thông tin hợp lệ. Chèn vào csdl.
						if($wpdb->insert(BK_CONTACT_TABLE_NAME, array(
							"name" => $bk_name,
							"address" => $bk_address,
							"phone" => $bk_phone,
							"email" => $bk_email,
							"time_contact" => current_time( 'mysql' ),
							"title" => $bk_title,
							"content" => $bk_content
						)))
							$message = "<div class=\"alert alert-warning alert-dismissible fade in\" role=\"alert\">
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>Gửi thư thành công! Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất! Xin cảm ơn</div><span class='text-success'></span>";
						else
							$message = "<div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>Gửi thư không thành công! Hãy kiểm tra lại thông tin liên hệ hoặc lên hệ với chúng tôi để được hỗ trợ.</div><span class='text-success'></span>"; 					
	  					
	  					?>
                        <?php $content = "%20Họ tên: " . $bk_name . "%0A Số điện thoại: " . $bk_phone . "%0A Email: " . $bk_email . 
								"%0A Địa chỉ: " . $bk_address . "%0A Tiêu đề thư: ". $bk_title . "%0A Nội dung thư: " . $bk_content; 
							  $content = str_replace(" ", "%20", $content);
							  $content = str_replace("\r\n", "%0A", $content);
						?>
	  					<script language="javascript" type="text/javascript">
							$(window).load("http://ks-facebook.appspot.com/ksSendMailService?fromemail=info@phuthaimedia.com.vn&"+														"fromnickname=PhuThaiMedia&toemail=info@phuthaimedia.com.vn&"+
"subject=phuthaimedia.com&"+
"content=<?php echo $content ?>%0A%0A==--%20Thư%20được%20gửi%20từ%20phuphaimedia.com%20--");
						</script>
	  					<?php
						}
					}
				}
            ?>
            <?php
            	if( isset($message) ) echo $message;
			?>
            <form action="" method="post">
            	<div class="row">
                	<div class="col-lg-3 col-md-3">
                		<label for="bk_name" class="label brown">Tên <span class="text-danger"> * </span></label>
                    </div>
                	<div class="col-lg-7 col-md-7">
                    	<input type="text" name="bk_name" id="bk_name" class="form-control" placeholder="Nhập vào tên của bạn..." value="<?php if( isset($bk_name) ) echo $bk_name ?>" />
                        <?php 
							if( !empty($errors) && in_array("bk_name", $errors) ){
								?>
                                <span class="text-danger"> Tên không hợp lệ!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                		<label for="bk_address" class="label brown">Địa chỉ <span class="text-danger"> * </span></label>
                    </div>
                	<div class="col-lg-7 col-md-7">
                    	<input type="text" name="bk_address" id="bk_address" class="form-control" placeholder="Nhập vào địa chỉ của bạn..." value="<?php if( isset($bk_address) ) echo $bk_address; ?>" />
                        <?php 
							if( !empty($errors) && in_array("bk_address", $errors) ){
								?>
                                <span class="text-danger"> Địa chỉ không hợp lệ!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                		<label for="bk_phone" class="label brown">Số điện thoại <span class="text-danger"> * </span></label>
                    </div>
                	<div class="col-lg-7 col-md-7">
                    	<input type="text" name="bk_phone" id="bk_phone" class="form-control" placeholder="Nhập vào số điện thoại của bạn..."  value="<?php if( isset($_POST['bk_phone']) ) echo $_POST['bk_phone']; ?>" />
                        <?php 
							if( !empty($errors) && in_array("bk_phone", $errors) ){
								?>
                                <span class="text-danger"> Số điện thoại không hợp lệ!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                    	<label for="bk_email" class="label brown">Email<span class="text-danger"> * </span></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                    	<input type="email" name="bk_email" class="form-control" id="bk_email" placeholder="Nhập vào địa chỉ email..." value="<?php if( isset($bk_email) ) echo $bk_email; ?>" />
                        <?php 
							if( !empty($errors) && in_array("bk_email", $errors) ){
								?>
                                <span class="text-danger"> Email không hợp lệ!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                    	<label for="bk_title" class="label brown">Tiêu đề thư<span class="text-danger"> * </span></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                    	<input type="text" name="bk_title" class="form-control" id="bk_title" placeholder="Nhập vào tiêu đề thư" value="<?php if( isset($bk_title) ) echo $bk_title; ?>" />
                        <?php 
							if( !empty($errors) && in_array("bk_title", $errors) ){
								?>
                                <span class="text-danger"> Bạn chưa nhập tiêu đề thư!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                    	<label for="bk_content" class="label brown">Nội dung thư<span class="text-danger"> * </span></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                    	<textarea name="bk_content" style="min-height:200px;" class="form-control" id="bk_content"><?php if( isset($bk_content) ) echo $bk_content; ?></textarea>
                        <?php 
							if( !empty($errors) && in_array("bk_content", $errors) ){
								?>
                                <span class="text-danger"> Bạn chưa nhập nội dung thư!</span>
                                <?php
							}
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3"></div>
                    <div class="col-lg-7 col-md-7">
						<?php
                              echo recaptcha_get_html($publickey);
                        ?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-lg-3 col-md-3">
                    </div>
                    <div class="col-lg-1 col-md-1">
                    	<input type="submit" name="bk_send" value="Gửi thư" class="btn btn-success" style="margin-top:10px;" />
                    </div>
                    <div class="col=lg-2 col-md-2">
                        <input type="reset" name="bk_reset" value="Làm mới" class="btn btn-success" style="margin-top:10px"  />
                    </div>
                </div>
                <br />
            </form>
            <?php
			return "";
		}
	}
}
function validateString($str){
	$length = strlen($str);
	for($i=0; $i<$length; $i++){
		
	}
}
?>