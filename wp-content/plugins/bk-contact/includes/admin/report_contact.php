<?php
/***************** Admin Page *******************************/
function bk_contact_register_mysettings() {
    register_setting( 'mfpd-settings-group', 'mfpd_option_name' );
}
function bk_contact_create_menu() {
    add_menu_page('Thông kê các thư liên hệ', 'Liên hệ', 'administrator', __FILE__, 'bk_contact_repost', 0);
    add_action( 'admin_init', 'bk_contact_register_mysettings' );
}
add_action('admin_menu', 'bk_contact_create_menu'); 

function bk_contact_repost() {
?>
<div class="container">
	<?php 
	?>
    
	<h3 class="text-success" style="margin:0 auto;margin-top:20px">Thông kê liên hệ</h3>
    <?php
		global $wpdb;
		
		//Kiểm tra xóa.
		if( isset($_GET['ac'], $_GET['pID'], $_GET['p']) && $_GET['ac'] == "delete" 
			&& filter_var($_GET['pID'], FILTER_VALIDATE_INT) && filter_var($_GET['p'], FILTER_VALIDATE_INT) ) {
			$my_query = "DELETE FROM " . $wpdb->prefix . "bk_contact WHERE id = " . $_GET['pID'] . " LIMIT 1";
			$wpdb->query($my_query);
		}
    	//Pagination
			/*Lấy số lượng thư liên hệ trong database*/
			$my_query = "SELECT COUNT(id) AS count_page FROM " . $wpdb->prefix . "bk_contact";
			$count_page = $wpdb->get_results($my_query);
			$count_page = $count_page[0];
			$count_page = $count_page->count_page;
			
			if( $count_page % 10 != 0 )
				$count_page = (int)($count_page / 10) + 1;
			else {
				$count_page = $count_page / 10;
			}
			
			/*Current page*/
			if( isset($_GET['p']) && filter_var($_GET['p'], FILTER_VALIDATE_INT) ) {
				$p = $_GET['p'];
				$limit = " LIMIT " . ($_GET['p'] - 1) * 10 . "," . $_GET['p'] * 10;
			} else {
				$p = 1;
				$limit = " LIMIT 0,10 ";
			}
	?>
		<ul class="pagination" style="float:right;margin:10px;">
        	<li <?php if( $p == 1 ) echo "class=\"disabled\"";  ?>>
      			<a href="<?php if( $p > 1 ) { ?>admin.php?page=bk-contact/includes/admin/report_contact.php&p=<?php echo $p - 1; } else echo "#";  ?>" aria-label="Previous">
        			<span aria-hidden="true">&laquo;</span>
      			</a>
    		</li>
        	<?php
            	for($i = 1; $i <= $count_page ; $i++) {
					?>
					<li <?php if( $p == $i ) echo "class='active'"; ?>>
                    	<a href="admin.php?page=bk-contact/includes/admin/report_contact.php&p=<?php echo $i ?>">
						<?php echo $i ?>
    	                </a>
                    </li>
                    <?php
				}
			?>
    		<li <?php if( $p == $count_page ) echo "class=\"disabled\"";  ?>>
		        <a href="<?php if( $p < $count_page ) { ?>admin.php?page=bk-contact/includes/admin/report_contact.php&p=<?php echo $p + 1; } else echo "#";  ?>" aria-label="Next">
	        		<span aria-hidden="true">&raquo;</span>
    			</a>
    		</li>
  		</ul>
    <table class="table table-bordered">
      <thead>
        <tr class="info">
          <th style="text-align:center">ID</th>
          <th style="text-align:center">Họ Tên</th>
          <th style="text-align:center">Địa chỉ</th>
          <th style="text-align:center">Số điện thoại</th>
          <th style="text-align:center">Email</th>
          <th style="text-align:center">Tiêu đề thư</th>
          <th style="text-align:center">Nội dung thư</th>
          <th style="text-align:center">Thời gian gửi thư</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody style="max-height:100px;overflow:scroll;height:auto;">
      	<?php
			//Truy xuất cơ sở dữ liệu.
			$my_query = "SELECT * FROM " . $wpdb->prefix . "bk_contact" . " ORDER BY id DESC " . $limit;
			$my_results = $wpdb->get_results($my_query);
			foreach( $my_results as $my_result ) {
				?>
                <tr>
                   	<th style="text-align:center"><?php echo $my_result->id; ?></th>
                   	<td><?php echo $my_result->name; ?></td>
		          	<td><?php echo $my_result->address; ?></td>
        		  	<td><?php echo $my_result->phone; ?></td>
                    <td><?php echo $my_result->email; ?></td>
                    <td><?php echo $my_result->title; ?></td>
                    <td><?php echo bk_contact_the_excerpt($my_result->id,$my_result->content, $my_result->title, $my_result->name, $my_result->phone, $my_result->email, $my_result->address, date_format(date_create($my_result->time_contact), "H:i:s d/m/Y") ); ?></td>
          			<td style="text-align:center"><?php echo date_format(date_create($my_result->time_contact), "H:i:s d/m/Y") ?></td>
                    <td>
                    	<a onclick="return confirm('Bạn có chắc chắn muốn xóa thư này không?')" href="admin.php?page=bk-contact/includes/admin/report_contact.php&p=<?php echo $p ?>&ac=delete&pID=<?php echo $my_result->id; ?>" title="Xóa thư">Xóa thư</a>
                    </td>
                </tr>
                <?php
			}
		?>
      </tbody>
    </table>
</div>
<?php echo '<link rel="stylesheet" type="text/css" href="' . plugins_url("../../css/style.css", __FILE__) . '" />'; ?>
<?php echo '<script language="javascript" type="text/javascript" src="'. plugins_url("../../js/template.min.js", __FILE__) .'"></script>'; ?>
<?php 
}