<?php
function bk_contact_the_excerpt($id, $excerpt, $title, $name, $phone, $email, $address, $date, $charlength = 200) {
	$content = $excerpt;
	$charlength++;
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		?>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-sm"  title="Đọc tiếp" data-toggle="modal" data-target="#myModal<?php echo $id ?>">
 		 	...
		</button>
        <!-- Modal -->
        <div class="modal fade" id="myModal<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel"><?php echo $title ?></h2>
              </div>
              <div class="modal-body">
              	<b>
					<?php echo "Họ tên: " . $name; ?>
                    <br />
                    <?php echo "Email: " . $email ?>
                    <br />
                    <?php echo "Số điện thoại: " . $phone ?>
                    <br />
                    <?php echo "Địa chỉ: " . $address ?>
                    <br />
                	<?php echo "Thời gian nhận thư: " . $date ?>
                	<br />	
                    <br />
                </b>
					<?php echo $content ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <?php
	} else {
		echo $excerpt;
	}
	?>
    <?php
}