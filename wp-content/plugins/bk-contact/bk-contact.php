<?php
/**
*	Plugin Name: Liên hệ
*	Plugin URI: http://bk-webs.com
*	Version: 1.0
*	Description: Plugin có cung cấp 1 shortcode ([bk-contact]) tạo trang liên hệ. Và trang thông kê các liên hệ trong trang quản trị.
*	Author: toanpham
*	Author URI: http://toanpham.tk
*	Text Domain: bk-contact
*
*/
/* Check version */
global $wp_version;
if( version_compare($wp_version, '3.8.0', '<')) {
	exit("Plugin chỉ phù hợp cho wordpress phiên bản 3.8.1 trở lên! Hãy update wordpress để sử dụng!");
}

include_once("includes/bk-contact-functions.php");

include_once("includes/admin/create_database.php");
global $wpdb;
define("BK_CONTACT_TABLE_NAME", $wpdb->prefix . "bk_contact");
function bk_contact_create_datebase() {	
	global $wpdb;
	if($wpdb->get_var("SHOW TABLES LIKE " . BK_CONTACT_TABLE_NAME ) != BK_CONTACT_TABLE_NAME) {
		$sql = "CREATE TABLE " . BK_CONTACT_TABLE_NAME . "(
			id INT UNSIGNED NOT NULL AUTO_INCREMENT,
			name VARCHAR(200) NOT  NULL,
			phone VARCHAR(11) NOT NULL,
			email VARCHAR(100) NOT NULL,
			address VARCHAR(500) NOT NULL,
			title VARCHAR(300) NOT NULL,
			content TEXT NOT NULL,
			time_contact DATETIME NOT NULL,
			CONSTRAINT PRIMARY KEY (id)
		)";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
register_activation_hook( __FILE__, 'bk_contact_create_datebase' );


/**********class BK_contact**************/
include_once("includes/class-bk-contact.php");
/*########################*/

function bk_contact_load() {
    global $mfpd;
	$bk_contact = new BK_contact();
}
add_action( 'plugins_loaded', 'bk_contact_load' );
/*#############Finish for form book a place############*/

/***************** Admin Page *******************************/
include_once("includes/admin/report_contact.php");
/*###########################################*/
?>