<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'phuthaimedia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/* define */
define('WP_HOME','http://localhost/phuthaimedia.com/');
define('WP_SITEURL','http://localhost/phuthaimedia.com/');

define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vjF![<DPO%>2)R$Xj9;z^Ci}prIoS_MQAZICT[A1`L7.52{O)P]?5PHhqSyINSC1');
define('SECURE_AUTH_KEY',  '80@OaeYY))G<CV)L!/T[.+R1~1toNca/=HE#N=l9>_80kO]&^Rgn! gx0uktu}H1');
define('LOGGED_IN_KEY',    'x{1je:(NPF0.uCjBB=oVXJDEL6`fE@uh:0UKMgSs.@,Q{|yv|HM6e2#mAsLa+EwF');
define('NONCE_KEY',        '$-,<cT(mTs0XT;.rJu^6DcEn*bF-}cdC@T]Fksa3&oAo&`y?ptsy%V!)iwVBn/-O');
define('AUTH_SALT',        'qhh3aCu0j+rf4S#vcU,t(fB>MZja75ZMDtrM7V0DY+QDE<;>3m[yHS`;MP|f*A1e');
define('SECURE_AUTH_SALT', '2iZA_CgpH$}z}$)YOOdoT;u,adH(%kyTI(!woi+gg=[mHsA=ztEY7deK8 N:+m,E');
define('LOGGED_IN_SALT',   '1,NH#lCiMjHCWw*F=*:vOe5](&Yb^qJv*9WJ:ki#6L0sy2<~ECZT4BC1Lo&!UsZi');
define('NONCE_SALT',       'U#y/;SfGPw`{B-,Nrh%d,Y4N3=] x[gpD)tdW>89EHIgr YK7L5oh#f(39|&Ur16');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'vi_VN');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
